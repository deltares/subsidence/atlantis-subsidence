"""
Module to hold general SoilColumn methods

Methods are used by both KoppejanSoilColumn and IsotacheSoilColumn
"""

from typing import NamedTuple
import numpy as np
import numba
from numba import int16, int8, int64, float64, bool_, int32


@numba.njit
def discretize(array, nlayer, sublayers):
    out = np.empty(nlayer)
    count = 0
    for i in range(sublayers.size):
        n = sublayers[i]
        out[count : count + n] = array[i]
        count += n
    return out


class ColumnInput(NamedTuple):
    thickness: np.ndarray
    zbase: float
    surface_level: float
    phreatic_level: float


class ColumnSurcharge(NamedTuple):
    thickness: np.ndarray


class WaterTableLowering(NamedTuple):
    phreatic_lowering: float
    aquifer_lowering: float
    track_subsidence: bool


column_empty_surcharge = ColumnSurcharge(thickness=np.array([[0.0]]))

# These are the states needed by ANY kind of soil column
column_spec = [
    ("zbase", float64),
    ("z", float64[:]),
    ("thickness", float64[:]),
    ("initial_thickness", float64[:]),
    ("surface_level", float64),
    ("phreatic_level", float64),
    ("aquifer_head", float64),
    ("subsidence", float64),
    ("time", float64),
    ("previous_time", float64),
    # Phreatic level change
    ("phreatic_lowering", float64),
    ("aquifer_lowering", float64),
    # Results, these always exist
    ("oxidation", float64[:]),
    ("consolidation", float64[:]),
    # Surcharge data
    ("surcharge_thickness", float64[:]),
    # Flag to add surcharge in coming stress period
    ("_use_surcharge", bool_),
]


class ColumnBase:
    """
    Timestepping methods, bookkeeping.

    Attributes
    ----------
    z : np.array of floats
        Midpoint coordinates
    thickness : np.array of floats
        Thickness of voxel layers (m)
    zbase : np.array of floats
        Base reference level of the voxel column (m)
    phreatic_level : float
        Level of the phreatic water table (m)
    surcharge_thickness : np.array of floats
        Thickness of voxel layers to apply as sucharge (m)
    """

    # =================
    # Utility functions
    # =================
    # Note: numba does not yet support @staticmethod
    def _split_array(self, array, index, divide, ratio):
        """Should only be called at the start of a stress period!"""
        if divide:
            v = array[index]
            v1 = ratio * v
            v2 = v - v1
        else:
            v1 = v2 = array[index]
        nlayer = array.size + 1
        new_array = np.empty(nlayer, dtype=array.dtype)
        new_array[:index] = array[:index]
        new_array[index + 1 :] = array[index:]
        new_array[index] = v1
        new_array[index + 1] = v2
        return new_array

    def _surcharge_array(self, array, to_add, nlayer_old, nlayer_new):
        new_array = np.empty(nlayer_new, dtype=array.dtype)
        new_array[:nlayer_old] = array[:]
        new_array[nlayer_old:nlayer_new] = to_add
        return new_array

    def check_size(self, array, size):
        return array.size == size

    def check_positive_scalar(self, scalar):
        return scalar >= 0

    def check_positive_array(self, array):
        """
        Checks whether the array is properly sized and has positive values.
        """
        for e in array:
            if np.isnan(e):
                continue
            if not e >= 0:
                return False
        return True

    # ==============
    # Initialization
    # ==============
    def __init__(self, column_input, consolidation_input=(), oxidation_input=()):
        # Pre-allocate arrays
        nlayer = column_input.thickness.size
        self._initialize_column(column_input)
        # Initialize consolidation and oxidation specific variables
        self._initialize_stress(nlayer)
        self._initialize_consolidation(consolidation_input)
        self._initialize_oxidation(oxidation_input)
        # Run validity checks on inputs
        self.check_column()
        self.check_consolidation()
        self.check_oxidation()

    def _initialize_column(self, column_input):
        # Array states
        thickness = column_input.thickness
        nlayer = thickness.size
        self.thickness = thickness
        self.initial_thickness = thickness.copy()
        self.z = column_input.zbase + np.cumsum(thickness) - 0.5 * thickness
        self.consolidation = np.zeros(nlayer)
        self.oxidation = np.zeros(nlayer)
        # Scalar states
        self.zbase = column_input.zbase
        self.phreatic_level = column_input.phreatic_level
        self.aquifer_head = np.nan
        self.surface_level = column_input.surface_level
        self.phreatic_lowering = 0.0
        self.aquifer_lowering = 0.0
        self.subsidence = 0.0
        self.time = 0.0
        self.previous_time = 0.0
        self._use_surcharge = False
        # Do a basic check on the inputs
        self.check_column()

    def check_column(self):
        size = self.thickness.size
        if not self.check_positive_array(self.thickness):
            raise ValueError("thickness must be positive")
        if self.phreatic_level > self.surface_level:
            raise ValueError("phreatic level larger than surface level")
        if self.phreatic_level < self.zbase:
            raise ValueError("phreatic level smaller than zbase")

    def _split_column(self, index, ratio, nlayer):
        self.thickness = self._split_array(self.thickness, index, True, ratio)
        self.initial_thickness = self.thickness.copy()
        self.z = self.zbase + np.cumsum(self.thickness) - 0.5 * self.thickness
        self.consolidation = np.zeros(nlayer)
        self.oxidation = np.zeros(nlayer)

    def _set_column_surcharge(self, surcharge=column_empty_surcharge):
        self.surcharge_thickness = surcharge.thickness

    def _surcharge_column(self, n_old, n_new):
        self.thickness = self._surcharge_array(
            self.thickness, self.surcharge_thickness, n_old, n_new
        )
        self.initial_thickness = self.thickness.copy()
        self.z = self.zbase + np.cumsum(self.thickness) - 0.5 * self.thickness
        self.consolidation = self._surcharge_array(
            self.consolidation, 0.0, n_old, n_new
        )
        self.oxidation = self._surcharge_array(self.oxidation, 0.0, n_old, n_new)

    # =====================
    # Stress period forcing
    # =====================
    def lower_watertable(self):
        self.phreatic_level -= self.phreatic_lowering
        self.aquifer_head -= self.aquifer_lowering

    def apply_surcharge(self):
        n_old = self.thickness.size
        n_new = n_old + self.surcharge_thickness.size
        self._surcharge_column(n_old, n_new)
        self._surcharge_consolidation(n_old, n_new)  # from consolidation process
        self._surcharge_oxidation()  # from oxidation process
        self.surface_level += self.surcharge_thickness.sum()
        # Perform checks
        self.check_column()
        self.check_consolidation()
        self.check_oxidation()
        self._use_surcharge = False

    def remove_surcharge(self, thickness):
        """
        Parameters
        ----------
        thickness : float
            Thickness to remove from soil column.
        """
        raise NotImplementedError

    # ==============================
    # Stress period and timestepping
    # ==============================
    def advance_timestep(self, timestep):
        """
        Advance by one timestep of specified duration.

        Parameters
        ----------
        timestep : float
            Current timestep
        """
        self.previous_time = self.time
        self.time += timestep
        # Make sure z is up to date
        self.z = self.zbase + np.cumsum(self.thickness) - 0.5 * self.thickness

        # Update stress
        self.f_timestep_stress()
        # Compute consolidation and oxidation
        self.f_consolidation()
        self.f_oxidation(timestep)

        # Compute subsidence for entire column
        subsidence = self.consolidation + self.oxidation
        # Ensure subsidence does not exceed thickness of cell
        # Since consolidation and oxidation are computed separately
        for i in range(self.thickness.size):
            if subsidence[i] > self.thickness[i]:
                subsidence[i] = self.thickness[i]

        # update states
        # consolidation increases density (gamma), oxidation does not
        self.f_update_gamma()
        # Finally, update thickness
        self.thickness = self.thickness - subsidence
        # Store accumulated subsidence
        self.subsidence = np.sum(subsidence)
        self.surface_level -= self.subsidence

    def advance_stressperiod(self, timesteps):
        """
        Advance a single stress period. During a stress period, all boundary
        conditions are constant.

        Multiple timesteps are taken within the stress period to compute
        model state. The length of these timesteps are defined with `timesteps`.

        Parameters
        ----------
        timesteps : np.array of floats
            Durations of the timesteps
        lowering : custom np.dtype
            with fields:
                * ("phreatic_lowering": np.float64)
                * ("aquifer_lowering": np.float64)
                * ("track_subsidence", np.bool)
        consolidation_surcharge : custom np.dtype
            with appropriate fields for the column type (koppejan or isotache)
        oxidation_surcharge : custom np.dtype
            with appropriate fields for the column type

        Returns
        -------
        total_consolidation : float
            Total consolidation in this stress period
        total_oxidation : float
            Total oxidation in this stress period
        total_subsidence : float
            Total subsidence due to consolidation and oxidation for the entire
            soil column
        """
        self.surface_level = self.zbase + np.sum(self.thickness)
        self.z = self.zbase + np.cumsum(self.thickness) - 0.5 * self.thickness
        self.f_oxidation_depth()
        self.split_phreatic_cell()
        self.initial_thickness[:] = self.thickness
        self.f_period_stress()
        # print(self.previous_effective_stress)

        # Apply forcing
        self.lower_watertable()
        if self._use_surcharge:
            self.apply_surcharge()

        # Initialize at 0
        total_subsidence = 0.0
        total_consolidation = 0.0
        total_oxidation = 0.0
        self.time = 0.0
        self.previous_time = 0.0
        self.subsidence = 0.0
        for timestep in timesteps:
            self.advance_timestep(timestep)
            # TODO: total consolidation and oxidation might not match subsidence exactly
            # the reason being that both are computed separately, and clamped to cell
            # thickness. This means together they may be two times cell thickness!
            total_consolidation += self.consolidation.sum()
            total_oxidation += self.oxidation.sum()
            total_subsidence += self.subsidence

        return total_consolidation, total_oxidation, total_subsidence

    def set_surcharge(
        self, column_surcharge, consolidation_surcharge, oxidation_surcharge
    ):
        self._set_column_surcharge(column_surcharge)
        self._set_consolidation_surcharge(consolidation_surcharge)
        self._set_oxidation_surcharge(oxidation_surcharge)
        self._use_surcharge = True

    def select_column_input(self, column_input, thickness, index):
        return ColumnInput(
            thickness=thickness,
            zbase=column_input.zbase[index],
            surface_level=column_input.surface_level[index],
            phreatic_level=column_input.phreatic_level[index],
        )


def run_stress_periods(
    column,
    period_timesteps,
    water_table_lowering=None,
    surcharge=None,
):
    # Prepare transient output arrays
    nperiods = len(period_timesteps)
    consolidation_t = np.full(nperiods + 1, 0.0)
    oxidation_t = np.full(nperiods + 1, 0.0)
    subsidence_t = np.full(nperiods + 1, 0.0)

    # In case of track_subsidence, lowering is based on previous subsidence
    subsidence = 0.0
    track_subsidence = False

    # Run stress periods
    for i in range(nperiods):
        timesteps = period_timesteps[i]

        # Reset lowering
        column.lowering = 0.0
        # Look up time dependent data.
        if water_table_lowering is not None:
            if i in water_table_lowering.keys():
                track_subsidence = water_table_lowering[i].track_subsidence
                column.phreatic_lowering = water_table_lowering[i].phreatic_lowering
                column.aquifer_lowering = water_table_lowering[i].aquifer_lowering

        if track_subsidence:
            column.phreatic_lowering += subsidence
            column.aquifer_lowering += subsidence

        # Look up time dependent data.
        if surcharge is not None:
            if i in surcharge.keys():
                (
                    column_surcharge,
                    consolidation_surcharge,
                    oxidation_surcharge,
                ) = surcharge[i]
                column.set_surcharge(
                    column_surcharge, consolidation_surcharge, oxidation_surcharge
                )

        consolidation, oxidation, subsidence = column.advance_stressperiod(timesteps)

        # all are cumulative
        consolidation_t[i + 1] = consolidation_t[i] + consolidation
        oxidation_t[i + 1] = oxidation_t[i] + oxidation
        subsidence_t[i + 1] = subsidence_t[i] + subsidence

    return consolidation_t, oxidation_t, subsidence_t
