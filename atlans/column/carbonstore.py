from typing import NamedTuple

from numba import int16, int8, int64, float64, bool_, int32
import numpy as np
import math

from .column_base import discretize
from .oxidation_base import OxidationBase


class CarbonStoreInput(NamedTuple):
    """
    Attributes
    ----------
    capillary_rise: float

    mass_fraction_organic: np.ndarray

    maximum_oxidation_depth: float
        (m)
    minimal_mass_fraction_organic: float
        (-)
    oxidation_rate_alpha : np.ndarray
        α (kg/m^2/d)
    rho_bulk: np.ndarray
        (-)
    """

    capillary_rise: float
    mass_fraction_organic: np.ndarray
    maximum_oxidation_depth: float
    minimal_mass_fraction_organic: float
    oxidation_rate_alpha: np.ndarray
    rho_bulk: np.ndarray


class CarbonStoreSurcharge(NamedTuple):
    """
    Attributes
    ----------
    mass_fraction_organic: np.ndarray
    oxidation_rate_alpha: np.ndarray
    rho_bulk: np.ndarray
    """

    mass_fraction_organic: np.ndarray
    oxidation_rate_alpha: np.ndarray
    rho_bulk: np.ndarray


carbon_store_empty_surcharge = CarbonStoreSurcharge(
    mass_fraction_organic=np.array([[0.0]]),
    oxidation_rate_alpha=np.array([[0.0]]),
    rho_bulk=np.array([[0.0]]),
)

carbon_store_spec = [
    ("capillary_rise", float64),
    ("mass_fraction_organic", float64[:]),
    ("mass_organic", float64[:]),
    ("mass_mineral", float64[:]),
    ("maximum_oxidation_depth", float64),
    ("minimal_mass_organic", float64[:]),
    ("minimal_mass_fraction_organic", float64),
    ("oxidation_depth", float64),
    ("oxidation_rate_alpha", float64[:]),
    ("rho_bulk", float64[:]),
    (
        "specific_volume",
        float64[:],
    ),
    # surcharge input
    ("surcharge_mass_fraction_organic", float64[:]),
    ("surcharge_oxidation_rate_alpha", float64[:]),
    ("surcharge_rho_bulk", float64[:]),
]


class CarbonStoreOxidation(OxidationBase):
    def _initialize_oxidation(
        self,
        oxidation_input,
    ):
        self.oxidation_rate_alpha = oxidation_input.oxidation_rate_alpha
        self.minimal_mass_fraction_organic = (
            oxidation_input.minimal_mass_fraction_organic
        )
        self.capillary_rise = oxidation_input.capillary_rise
        self.rho_bulk = oxidation_input.rho_bulk
        self.mass_fraction_organic = oxidation_input.mass_fraction_organic
        mass_fraction_organic = oxidation_input.mass_fraction_organic
        self.mass_organic = self.f_mass_organic(
            mass_fraction_organic, self.rho_bulk, self.thickness
        )
        self.mass_mineral = self.f_mass_mineral(
            mass_fraction_organic, self.rho_bulk, self.thickness
        )
        self.minimal_mass_organic = self.f_minimal_mass_organic(
            self.mass_mineral, self.minimal_mass_fraction_organic
        )
        self.maximum_oxidation_depth = oxidation_input.maximum_oxidation_depth
        nlayer = self.thickness.size
        self.specific_volume = np.zeros(nlayer)
        self.update_specific_volume()

    def check_oxidation(self):
        size = self.thickness.size
        # Check scalars
        if not self.check_positive_scalar(self.capillary_rise):
            raise ValueError("capillary_rise must be positive")
        if not self.check_positive_scalar(self.maximum_oxidation_depth):
            raise ValueError("maximum_oxidation_depth must be positive")
        if not self.check_positive_scalar(self.minimal_mass_fraction_organic):
            raise ValueError("minimal_mass_fraction_organic must be positive")
        # Check arrays
        # Sizes
        if not self.check_size(self.mass_fraction_organic, size):
            raise ValueError("mass_fraction_organic size mismatch")
        if not self.check_size(self.oxidation_rate_alpha, size):
            raise ValueError("oxidation_rate_alpha size mismatch")
        if not self.check_size(self.rho_bulk, size):
            raise ValueError("rho_bulk size mismatch")
        # Positive values
        if not self.check_positive_array(self.mass_fraction_organic):
            raise ValueError("mass_fraction_organic must be positive")
        if not self.check_positive_array(self.oxidation_rate_alpha):
            raise ValueError("oxidation_rate_alpha must be positive")
        if not self.check_positive_array(self.rho_bulk):
            raise ValueError("rho_bulk must be positive")

    def f_mass_organic(self, mass_fraction_organic, rho_bulk, thickness):
        return mass_fraction_organic * rho_bulk * thickness

    def f_mass_mineral(self, mass_fraction_organic, rho_bulk, thickness):
        return (1.0 - mass_fraction_organic) * rho_bulk * thickness

    def f_minimal_mass_organic(self, mass_mineral, minimal_mass_fraction_organic):
        return (
            mass_mineral
            * minimal_mass_fraction_organic
            / (1.0 - minimal_mass_fraction_organic)
        )

    def _set_oxidation_surcharge(
        self, oxidation_surcharge=carbon_store_empty_surcharge
    ):
        self.surcharge_mass_fraction_organic = oxidation_surcharge.mass_fraction_organic
        self.surcharge_oxidation_rate_alpha = oxidation_surcharge.oxidation_rate_alpha
        self.surcharge_rho_bulk = oxidation_surcharge.rho_bulk

    def _surcharge_oxidation(self):
        # unpack
        mass_fraction_organic = self.surcharge_mass_fraction_organic
        oxidation_rate_alpha = self.surcharge_oxidation_rate_alpha
        rho_bulk = self.surcharge_rho_bulk
        # apply
        nlayer_old = self.oxidation_rate_alpha.size
        nlayer_new = nlayer_old + oxidation_rate_alpha.size
        self.oxidation_rate_alpha = self._surcharge_array(
            self.oxidation_rate_alpha, oxidation_rate_alpha, nlayer_old, nlayer_new
        )
        self.rho_bulk = self._surcharge_array(
            self.rho_bulk, rho_bulk, nlayer_old, nlayer_new
        )
        self.mass_fraction_organic = self._surcharge_array(
            self.mass_fraction_organic, mass_fraction_organic, nlayer_old, nlayer_new
        )
        # Only use new part
        thickness = self.thickness[nlayer_old:]
        mass_organic = self.f_mass_organic(mass_fraction_organic, rho_bulk, thickness)
        mass_mineral = self.f_mass_mineral(mass_fraction_organic, rho_bulk, thickness)
        minimal_mass_organic = self.f_minimal_mass_organic(
            mass_mineral, self.minimal_mass_fraction_organic
        )
        self.mass_organic = self._surcharge_array(
            self.mass_organic, mass_organic, nlayer_old, nlayer_new
        )
        self.mass_mineral = self._surcharge_array(
            self.mass_mineral, mass_mineral, nlayer_old, nlayer_new
        )
        self.minimal_mass_organic = self._surcharge_array(
            self.minimal_mass_organic, minimal_mass_organic, nlayer_old, nlayer_new
        )
        self.specific_volume = np.zeros(nlayer_new)

    def f_oxidation_depth(self):
        z1 = self.phreatic_level - self.phreatic_lowering + self.capillary_rise
        z2 = self.surface_level - self.maximum_oxidation_depth
        self.oxidation_depth = max(z1, z2)

    def _split_oxidation(self, index, ratio):
        self.oxidation_rate_alpha = self._split_array(
            self.oxidation_rate_alpha, index, False, ratio
        )
        self.rho_bulk = self._split_array(self.rho_bulk, index, False, ratio)
        self.mass_fraction_organic = self._split_array(
            self.mass_fraction_organic, index, False, ratio
        )
        self.mass_organic = self._split_array(self.mass_organic, index, True, ratio)
        self.mass_mineral = self._split_array(self.mass_mineral, index, True, ratio)
        self.minimal_mass_organic = self._split_array(
            self.minimal_mass_organic, index, True, ratio
        )
        self.specific_volume = self._split_array(
            self.specific_volume, index, False, ratio
        )

    def update_rho_bulk(self):
        self.rho_bulk = (self.mass_organic + self.mass_mineral) / self.thickness

    def update_mass_fraction_organic(self):
        self.mass_fraction_organic = self.mass_organic / (
            self.mass_organic + self.mass_mineral
        )

    def update_specific_volume(self):
        nlayer = self.thickness.size
        for i in range(nlayer):
            F_org = self.mass_fraction_organic[i]
            if F_org == 0.0:
                self.specific_volume[i] = 0.0
            else:
                R_bulk = self.rho_bulk[i]
                # No issues with 0: erf(0) == 0
                secondterm = 1.0 + math.erf((F_org - 0.2) / 0.1)
                self.specific_volume[i] = 0.5 / (F_org * R_bulk) * secondterm

    def f_oxidation(self, timestep):
        self.update_rho_bulk()
        self.update_mass_fraction_organic()
        self.update_specific_volume()

        self.oxidation[:] = 0.0
        nlayer = self.thickness.size
        for i in range(nlayer - 1, -1, -1):
            if self.z[i] > self.oxidation_depth:
                rate = self.oxidation_rate_alpha[i]
                if rate == 0.0:
                    continue
                dz = self.thickness[i]
                m = self.mass_organic[i]
                m_min = self.minimal_mass_organic[i]
                # Note: if m is less than m_min: m - m_min will be negative
                mass_change = min(m - m_min, rate * dz * timestep)
                # So check if it's above 0, if not, take 0
                mass_change = max(0.0, mass_change)
                self.mass_organic[i] -= mass_change
                self.oxidation[i] = self.specific_volume[i] * mass_change
            else:  # This means we've reached saturation, no need to go on
                break

    def select_oxidation_input(self, oxidation_input, nlayer, sublayers, index):
        return CarbonStoreInput(
            capillary_rise=oxidation_input.capillary_rise[index],
            mass_fraction_organic=discretize(
                oxidation_input.mass_fraction_organic[index], nlayer, sublayers
            ),
            maximum_oxidation_depth=oxidation_input.maximum_oxidation_depth[index],
            minimal_mass_fraction_organic=oxidation_input.minimal_mass_fraction_organic[
                index
            ],
            oxidation_rate_alpha=discretize(
                oxidation_input.oxidation_rate_alpha[index], nlayer, sublayers
            ),
            rho_bulk=discretize(oxidation_input.rho_bulk[index], nlayer, sublayers),
        )

    def select_oxidation_surcharge(
        self, oxidation_surcharge, nlayer, sublayers, period, index
    ):
        return CarbonStoreSurcharge(
            mass_fraction_organic=discretize(
                oxidation_surcharge.mass_fraction_organic[period, index],
                nlayer,
                sublayers,
            ),
            oxidation_rate_alpha=discretize(
                oxidation_surcharge.oxidation_rate_alpha[period, index],
                nlayer,
                sublayers,
            ),
            rho_bulk=discretize(
                oxidation_surcharge.rho_bulk[period, index], nlayer, sublayers
            ),
        )
