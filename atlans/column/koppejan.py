"""
Koppejan rekenmethode voor zetting gebaseerd op CUR Rapport 162
Construeren met grond. Zie parafraaf 5.7.2, pagina 126/411
en bijlages.

Regarding typing errors:

https://stackoverflow.com/questions/36278590/numpy-array-dtype-is-coming-as-int32-by-default-in-a-windows-10-64-bit-machine

Windows standaard int's op 64 bits machines zijn 32 bit ints.

All units within module are:
    * Time: days
    * Length: meter
    * Pressure: Pa (or N/m2)
    * Volumetric weight: Pa/m3
"""

from typing import NamedTuple

import numpy as np
from numba.experimental import jitclass
from numba import int16, int8, int64, float64, bool_, int32

from .column_base import discretize

from .consolidation_base import (
    ConsolidationBase,
    ConsolidationInput,
    ConsolidationSurcharge,
    consolidation_empty_surcharge,
)


class KoppejanInput(NamedTuple):
    gamma_wet: np.ndarray
    gamma_dry: np.ndarray
    c_v: np.ndarray
    drainage_factor: np.ndarray
    pop_ocr: np.ndarray
    aquifer_top: float
    aquifer_head: float
    preCp: np.ndarray
    preCs: np.ndarray
    postCp: np.ndarray
    postCs: np.ndarray
    pop_flag: bool


assert set(KoppejanInput._fields).issuperset(ConsolidationInput._fields)


class KoppejanSurcharge(NamedTuple):
    gamma_wet: np.ndarray
    gamma_dry: np.ndarray
    c_v: np.ndarray
    drainage_factor: np.ndarray
    pop_ocr: np.ndarray
    preCp: np.ndarray
    preCs: np.ndarray
    postCp: np.ndarray
    postCs: np.ndarray


assert set(KoppejanSurcharge._fields).issuperset(ConsolidationSurcharge._fields)


koppejan_empty_surcharge = KoppejanSurcharge(
    **consolidation_empty_surcharge._asdict(),
    preCp=np.array([[0.0]]),
    preCs=np.array([[0.0]]),
    postCp=np.array([[0.0]]),
    postCs=np.array([[0.0]]),
)

koppejan_spec = [
    ("preCp", float64[:]),
    ("preCs", float64[:]),
    ("postCp", float64[:]),
    ("postCs", float64[:]),
    ("surcharge_preCp", float64[:]),
    ("surcharge_preCs", float64[:]),
    ("surcharge_postCp", float64[:]),
    ("surcharge_postCs", float64[:]),
]


class KoppejanConsolidation(ConsolidationBase):
    """
    Attributes
    ----------
    preCp : np.array of floats
        Primary Pre-consolidation constant
    preCs : np.array of floats
        Secondary Pre-consolidation constant
    postCp : np.array of floats
        Primary Post-consolidation constant
    postCs : np.array of floats
        Secondary Post-consolidation constant
    """

    def _initialize_consolidation(self, consolidation_input):
        self._initialize_base_consolidation(consolidation_input)
        self.preCp = consolidation_input.preCp
        self.preCs = consolidation_input.preCs
        self.postCp = consolidation_input.postCp
        self.postCs = consolidation_input.postCs

    def _set_consolidation_surcharge(
        self, consolidation_surcharge=koppejan_empty_surcharge
    ):
        self._set_base_consolidation_surcharge(consolidation_surcharge)
        self.surcharge_preCp = consolidation_surcharge.preCp
        self.surcharge_preCs = consolidation_surcharge.preCs
        self.surcharge_postCp = consolidation_surcharge.postCp
        self.surcharge_postCs = consolidation_surcharge.postCs

    def _surcharge_consolidation(self, nlayer_old, nlayer_new):
        self._surcharge_base_consolidation(nlayer_old, nlayer_new)
        self.preCp = self._surcharge_array(
            self.preCp, self.surcharge_preCp, nlayer_old, nlayer_new
        )
        self.preCs = self._surcharge_array(
            self.preCs, self.surcharge_preCs, nlayer_old, nlayer_new
        )
        self.postCp = self._surcharge_array(
            self.postCp, self.surcharge_postCp, nlayer_old, nlayer_new
        )
        self.postCs = self._surcharge_array(
            self.postCs, self.surcharge_postCs, nlayer_old, nlayer_new
        )

    def _split_consolidation(self, index, ratio, nlayer):
        self._split_base_consolidation(index, ratio, nlayer)
        self.preCp = self._split_array(self.preCp, index, False, ratio)
        self.preCs = self._split_array(self.preCs, index, False, ratio)
        self.postCp = self._split_array(self.postCp, index, False, ratio)
        self.postCs = self._split_array(self.postCs, index, False, ratio)

    def check_consolidation(self):
        size = self.thickness.size
        if not self.check_positive_array(self.preCp):
            raise ValueError("preCp must be positive")
        if not self.check_positive_array(self.preCs):
            raise ValueError("preCs must be positive")
        if not self.check_positive_array(self.postCp):
            raise ValueError("postCp must be positive")
        if not self.check_positive_array(self.postCs):
            raise ValueError("postCs must be positive")

    def f_preyield_strain(self, i, time, previous_time) -> float:
        """
        * Skip if already beyond yield (preconsolidation) stress.
        * Compute until yield stress otherwise.
        * Skip secondary term if coefficient is 0.0.
        """
        if self.previous_effective_stress[i] > self.preconsolidation_stress[i]:
            # previous and current both exceed preconsolidation stress.
            return 0.0

        primary = self.delta_U[i] / self.preCp[i]
        if self.preCs[i] == 0:
            secondary = 0.0
        else:
            secondary = 1.0 / self.preCs[i] * np.log10(time / previous_time)

        # Take the lowest of effective stress and preconsolidation stress.
        new_stress = min(self.effective_stress[i], self.preconsolidation_stress[i])
        return (primary + secondary) * np.log(
            new_stress / self.previous_effective_stress[i]
        )

    def f_postyield_strain(self, i, time, previous_time) -> float:
        """
        * Skip if before yield (preconsolidation) stress.
        * Compute from yield stress or beyond otherwise.
        * Skip secondary term if coefficient is 0.0.
        """
        # Neither current not previous exceed preconsolidation stress.
        if self.effective_stress[i] < self.preconsolidation_stress[i]:
            return 0.0

        primary = self.delta_U[i] / self.postCp[i]
        if self.postCs[i] == 0:
            secondary = 0.0
        else:
            secondary = 1.0 / self.postCs[i] * np.log10(time / previous_time)

        # Take the highest of previous effective stress and preconsolidation stress.
        old_stress = max(
            self.previous_effective_stress[i], self.preconsolidation_stress[i]
        )
        return (primary + secondary) * np.log(self.effective_stress[i] / old_stress)

    def f_consolidation(self):
        """
        calculate consolidation by loading using koppejan's method.

        There are some conditions to consider:

        * Skip if layer thickness is zero.
        * Skip if primary coefficients are set to 0.
        * Skip if effective stress DECREASES.
        * Skip if previous effective stress is 0 (dry cell).

        """
        self.strain[:] = 0.0
        self.f_delta_U()

        nlayer = self.thickness.size
        # Logarithm of 0 is not defined. Add 1.0 so consolidation can be
        # computed in the first timestep (when previous_time = 0.0)
        time = self.time + 1.0
        previous_time = self.previous_time + 1.0

        for i in range(nlayer):
            if self.thickness[i] == 0.0:
                continue
            # skip voxel
            if (self.preCp[i] == 0.0) or (self.preCs[i] == 0.0):
                continue
            if self.effective_stress[i] <= self.previous_effective_stress[i]:
                continue
            if self.previous_effective_stress[i] == 0.0:
                continue

            pre = self.f_preyield_strain(i, time, previous_time)
            post = self.f_postyield_strain(i, time, previous_time)
            self.strain[i] = pre + post

        self.consolidation[:] = self.strain * self.thickness

    # Would've preferably been @staticmethod, but not supported by numba
    def select_consolidation_input(self, consolidation_input, nlayer, sublayers, index):
        return KoppejanInput(
            gamma_wet=discretize(
                consolidation_input.gamma_wet[index], nlayer, sublayers
            ),
            gamma_dry=discretize(
                consolidation_input.gamma_dry[index], nlayer, sublayers
            ),
            preCp=discretize(consolidation_input.preCp[index], nlayer, sublayers),
            preCs=discretize(consolidation_input.preCs[index], nlayer, sublayers),
            postCp=discretize(consolidation_input.postCp[index], nlayer, sublayers),
            postCs=discretize(consolidation_input.postCs[index], nlayer, sublayers),
            drainage_factor=discretize(
                consolidation_input.drainage_factor[index], nlayer, sublayers
            ),
            c_v=discretize(consolidation_input.c_v[index], nlayer, sublayers),
            pop_ocr=discretize(consolidation_input.pop_ocr[index], nlayer, sublayers),
            aquifer_top=consolidation_input.aquifer_top[index],
            aquifer_head=consolidation_input.aquifer_head[index],
            pop_flag=consolidation_input.pop_flag,
        )

    def select_consolidation_surcharge(
        self, consolidation_surcharge, nlayer, sublayers, period, index
    ):
        return KoppejanSurcharge(
            gamma_wet=discretize(
                consolidation_surcharge.gamma_wet[period, index], nlayer, sublayers
            ),
            gamma_dry=discretize(
                consolidation_surcharge.gamma_dry[period, index], nlayer, sublayers
            ),
            preCp=discretize(
                consolidation_surcharge.preCp[period, index], nlayer, sublayers
            ),
            preCs=discretize(
                consolidation_surcharge.preCp[period, index], nlayer, sublayers
            ),
            postCp=discretize(
                consolidation_surcharge.postCp[period, index], nlayer, sublayers
            ),
            postCs=discretize(
                consolidation_surcharge.postCs[period, index], nlayer, sublayers
            ),
            c_v=discretize(
                consolidation_surcharge.c_v[period, index], nlayer, sublayers
            ),
            drainage_factor=discretize(
                consolidation_surcharge.drainage_factor[period, index],
                nlayer,
                sublayers,
            ),
            pop_ocr=discretize(
                consolidation_surcharge.pop_ocr[period, index], nlayer, sublayers
            ),
        )
