import os
from typing import NamedTuple
from numba import types
from numba.experimental.jitclass.base import register_class_type, ClassBuilder

# Base column
from .column_base import (
    ColumnBase,
    column_spec,
    ColumnInput,
    WaterTableLowering,
    ColumnSurcharge,
    column_empty_surcharge,
    run_stress_periods,
)

# Consolidation imports
from .consolidation_base import (
    AbstractConsolidation,
    consolidation_spec,
    consolidation_empty_surcharge,
)
from .koppejan import (
    KoppejanConsolidation,
    koppejan_spec,
    KoppejanInput,
    KoppejanSurcharge,
    koppejan_empty_surcharge,
)
from .isotache import (
    IsotacheConsolidation,
    isotache_spec,
    IsotacheInput,
    IsotacheSurcharge,
    isotache_empty_surcharge,
)

# Oxidation imports
from .oxidation_base import AbstractOxidation
from .constant_rate import (
    ConstantRateOxidation,
    constant_rate_spec,
    ConstantRateInput,
    ConstantRateSurcharge,
    constant_rate_empty_surcharge,
)
from .carbonstore import (
    CarbonStoreOxidation,
    carbon_store_spec,
    CarbonStoreInput,
    CarbonStoreSurcharge,
    carbon_store_empty_surcharge,
)


class Nothing(NamedTuple):
    """
    Just an empty named tuple
    """


CONSOLIDATION_CONCEPTS = {
    # Add empty spec, no input fields, no surcharge: [], (), resp.
    type(None): (AbstractConsolidation, [], ()),
    Nothing: (AbstractConsolidation, [], ()),
    KoppejanInput: (
        KoppejanConsolidation,
        consolidation_spec + koppejan_spec,
        consolidation_empty_surcharge + koppejan_empty_surcharge,
    ),
    IsotacheInput: (
        IsotacheConsolidation,
        consolidation_spec + isotache_spec,
        consolidation_empty_surcharge + isotache_empty_surcharge,
    ),
}


OXIDATION_CONCEPTS = {
    type(None): (AbstractOxidation, [], ()),
    Nothing: (AbstractOxidation, [], ()),
    ConstantRateInput: (
        ConstantRateOxidation,
        constant_rate_spec,
        constant_rate_empty_surcharge,
    ),
    CarbonStoreInput: (
        CarbonStoreOxidation,
        carbon_store_spec,
        carbon_store_empty_surcharge,
    ),
}


INPUTS = {
    "koppejan": KoppejanInput,
    "isotache": IsotacheInput,
    "constant_rate": ConstantRateInput,
    "carbon_store": CarbonStoreInput,
    None: Nothing,
}

SURCHARGES = {
    "koppejan": KoppejanSurcharge,
    "isotache": IsotacheSurcharge,
    "constant_rate": ConstantRateSurcharge,
    "carbon_store": CarbonStoreSurcharge,
    None: Nothing,
}

EMPTY_SURCHARGES = {
    "column": column_empty_surcharge,
    "koppejan": koppejan_empty_surcharge,
    "isotache": isotache_empty_surcharge,
    "constant_rate": constant_rate_empty_surcharge,
    "carbon_store": carbon_store_empty_surcharge,
    None: Nothing(),
}


def create_soil_column_class(consolidation, oxidation):
    (
        consolidation_class,
        consolidation_spec,
        consolidation_surcharge,
    ) = CONSOLIDATION_CONCEPTS[consolidation]
    (
        oxidation_class,
        oxidation_spec,
        oxidation_surcharge,
    ) = OXIDATION_CONCEPTS[oxidation]
    # Dynamically combine requirements
    spec = column_spec + consolidation_spec + oxidation_spec

    try:
        compiled = os.environ["NUMBA_DISABLE_JIT"] == "0"
    except KeyError:
        compiled = True

    class Column(ColumnBase, consolidation_class, oxidation_class):
        # TODO: import docstrings?
        pass

    if compiled:
        Column = register_class_type(Column, spec, types.ClassType, ClassBuilder)

    return Column


def SoilColumn(column_input, consolidation_input, oxidation_input):
    Column = create_soil_column_class(type(consolidation_input), type(oxidation_input))
    if consolidation_input is None:
        consolidation_input = ()
    if oxidation_input is None:
        oxidation_input = ()
    return Column(column_input, consolidation_input, oxidation_input)


def create_input_selector(consolidation, oxidation):
    consolidation_class, _, _ = CONSOLIDATION_CONCEPTS[consolidation]
    oxidation_class, _, _ = OXIDATION_CONCEPTS[oxidation]
    try:
        compiled = os.environ["NUMBA_DISABLE_JIT"] == "0"
    except KeyError:
        compiled = True

    class InputSelector(ColumnBase, consolidation_class, oxidation_class):
        def __init__(self):
            pass

    if compiled:
        InputSelector = register_class_type(
            InputSelector, [], types.ClassType, ClassBuilder
        )

    return InputSelector
