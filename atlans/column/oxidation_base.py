class AbstractOxidation:
    """
    Minimum logic for an Oxidation class.
    """

    def _initialize_oxidation(self, *args):
        pass

    def check_oxidation(self, *args):
        pass

    def _set_oxidation_surcharge(self, *args):
        pass

    def _surcharge_oxidation(self, *args):
        pass

    def split_phreatic_cell(self, *args):
        pass

    def _split_oxidation(self, *args):
        pass

    def f_oxidation(self, *args):
        pass

    def f_oxidation_depth(self, *args):
        pass

    def select_oxidation_input(self, *args):
        pass

    def select_oxidation_surcharge(self, *args):
        pass


class OxidationBase(AbstractOxidation):
    def split_phreatic_cell(self):
        level = self.oxidation_depth
        nlayer = self.thickness.size
        split = False
        for i in range(nlayer):
            z = self.z[i]  # midpoint of cell
            dz = self.thickness[i]
            ztop = z + 0.5 * dz
            zbot = z - 0.5 * dz
            if (level < ztop) and (level > zbot):
                upper_difference = ztop - level
                lower_difference = level - zbot
                if (upper_difference < 0.01) or (lower_difference < 0.01):
                    ratio = 0.0
                    split = False
                    break
                else:
                    ratio = lower_difference / dz
                    split = True
                    break
        if split:
            nlayer += 1
            # Resize all arrays
            self._split_column(i, ratio, nlayer)
            self._split_consolidation(i, ratio, nlayer)
            self._split_oxidation(i, ratio)
