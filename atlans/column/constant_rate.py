from typing import NamedTuple

from numba import int16, int8, int64, float64, bool_, int32
import numpy as np
from .column_base import discretize
from .oxidation_base import OxidationBase


class ConstantRateInput(NamedTuple):
    oxidation_rate: np.ndarray
    maximum_oxidation_depth: float


class ConstantRateSurcharge(NamedTuple):
    oxidation_rate: np.array


constant_rate_empty_surcharge = ConstantRateSurcharge(oxidation_rate=np.array([[0.0]]))

constant_rate_spec = [
    ("oxidation_rate", float64[:]),
    ("maximum_oxidation_depth", float64),
    ("oxidation_depth", float64),
    ("surcharge_oxidation_rate", float64[:]),
]


class ConstantRateOxidation(OxidationBase):
    def _initialize_oxidation(self, oxidation_input):
        self.oxidation_rate = oxidation_input.oxidation_rate
        self.maximum_oxidation_depth = oxidation_input.maximum_oxidation_depth

    def check_oxidation(self):
        size = self.thickness.size
        if not self.check_size(self.oxidation_rate, size):
            raise ValueError("oxidation_rate size mismatch")
        if not self.check_positive_array(self.oxidation_rate):
            raise ValueError("oxidation rate must be positive")
        if not self.check_positive_scalar(self.maximum_oxidation_depth):
            raise ValueError("oxidation depth must be positive")

    def _set_oxidation_surcharge(
        self, oxidation_surcharge=constant_rate_empty_surcharge
    ):
        self.surcharge_oxidation_rate = oxidation_surcharge.oxidation_rate

    def _surcharge_oxidation(self):
        # unpack
        oxidation_rate = self.surcharge_oxidation_rate
        # apply
        nlayer_old = self.oxidation_rate.size
        nlayer_new = nlayer_old + oxidation_rate.size
        self.oxidation_rate = self._surcharge_array(
            self.oxidation_rate, oxidation_rate, nlayer_old, nlayer_new
        )

    def f_oxidation_depth(self):
        z1 = self.phreatic_level - self.phreatic_lowering
        z2 = self.surface_level - self.maximum_oxidation_depth
        self.oxidation_depth = max(z1, z2)

    def _split_oxidation(self, index, ratio):
        self.oxidation_rate = self._split_array(
            self.oxidation_rate, index, False, ratio
        )

    def f_oxidation(self, timestep):
        """
        Calculates oxidation of peat based on phreatic_levels, oxidation rate.
        Does not calculate oxidation deeper than 1.2 m below surface level.
        """
        nlayer = self.thickness.size
        self.oxidation[:] = 0.0

        for i in range(nlayer - 1, -1, -1):
            if self.z[i] > self.oxidation_depth:
                dz = self.thickness[i]
                change = dz * (1.0 - np.exp(-self.oxidation_rate[i] * timestep))
                self.oxidation[i] = min(dz, change)
            else:
                break

    def select_oxidation_input(self, oxidation_input, nlayer, sublayers, index):
        return ConstantRateInput(
            oxidation_rate=discretize(
                oxidation_input.oxidation_rate[index], nlayer, sublayers
            ),
            maximum_oxidation_depth=oxidation_input.maximum_oxidation_depth[index],
        )

    def select_oxidation_surcharge(
        self, oxidation_surcharge, nlayer, sublayers, period, index
    ):
        return ConstantRateSurcharge(
            oxidation_rate=discretize(
                oxidation_surcharge.oxidation_rate[period, index], nlayer, sublayers
            ),
        )
