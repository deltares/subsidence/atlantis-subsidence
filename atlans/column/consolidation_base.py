from typing import NamedTuple

from numba import int16, int8, int64, float64, bool_, int32
import numpy as np


class AbstractConsolidation:
    """
    Minimum logic for a Consolidation class.
    """

    def _initialize_stress(self, *args):
        pass

    def _initialize_consolidation(self, *args):
        pass

    def check_consolidation(self, *args):
        pass

    def _split_base_consolidation(self, *args):
        pass

    def _set_consolidation_surcharge(self, *args):
        pass

    def _surcharge_consolidation(self, *args):
        pass

    def _split_consolidation(self, *args):
        pass

    def f_period_stress(self, *args):
        pass

    def f_timestep_stress(self, *args):
        pass

    def f_consolidation(self, *args):
        pass

    def f_update_gamma(self, *args):
        pass


class ConsolidationInput(NamedTuple):
    gamma_wet: np.ndarray
    gamma_dry: np.ndarray
    c_v: np.ndarray
    drainage_factor: np.ndarray
    pop_ocr: np.ndarray
    aquifer_top: float
    aquifer_head: float
    pop_flag: bool


class ConsolidationSurcharge(NamedTuple):
    gamma_wet: np.ndarray
    gamma_dry: np.ndarray
    c_v: np.ndarray
    drainage_factor: np.ndarray
    pop_ocr: np.ndarray


consolidation_empty_surcharge = ConsolidationSurcharge(
    gamma_wet=np.array([[0.0]]),
    gamma_dry=np.array([[0.0]]),
    c_v=np.array([[0.0]]),
    drainage_factor=np.array([[0.0]]),
    pop_ocr=np.array([[0.0]]),
)

consolidation_spec = [
    ("aquifer_top", float64),
    ("aquifer_head", float64),
    ("pore_pressure", float64[:]),
    ("total_stress", float64[:]),
    ("effective_stress", float64[:]),
    ("previous_effective_stress", float64[:]),
    ("preconsolidation_stress", float64[:]),
    ("delta_U", float64[:]),
    ("strain", float64[:]),
    ("gamma_water", float64),
    ("gamma_wet", float64[:]),
    ("gamma_dry", float64[:]),
    ("c_v", float64[:]),
    ("drainage_factor", float64[:]),
    ("pop_ocr", float64[:]),
    ("pop_flag", bool_),
    # Surcharge data
    ("surcharge_gamma_wet", float64[:]),
    ("surcharge_gamma_dry", float64[:]),
    ("surcharge_c_v", float64[:]),
    ("surcharge_drainage_factor", float64[:]),
    ("surcharge_pop_ocr", float64[:]),
]


class ConsolidationBase(AbstractConsolidation):
    """
    Contains all the methods necessary for a Consolidation process,
    primarily computing stresses.

    Attributes are provided by ColumnBase
    """

    def _initialize_stress(self, nlayer):
        self.gamma_water = 9810.0
        self.pore_pressure = np.zeros(nlayer)
        self.total_stress = np.zeros(nlayer)
        self.effective_stress = np.zeros(nlayer)
        self.previous_effective_stress = np.zeros(nlayer)
        self.preconsolidation_stress = np.zeros(nlayer)
        self.delta_U = np.zeros(nlayer)
        self.strain = np.zeros(nlayer)

    def _initialize_base_consolidation(self, consolidation_input):
        self.gamma_wet = consolidation_input.gamma_wet
        self.gamma_dry = consolidation_input.gamma_dry
        self.c_v = consolidation_input.c_v
        self.drainage_factor = consolidation_input.drainage_factor
        self.pop_ocr = consolidation_input.pop_ocr
        self.pop_flag = consolidation_input.pop_flag
        self.aquifer_head = consolidation_input.aquifer_head
        self.aquifer_top = consolidation_input.aquifer_top

    def _set_base_consolidation_surcharge(self, consolidation_surcharge):
        self.surcharge_gamma_wet = consolidation_surcharge.gamma_wet
        self.surcharge_gamma_dry = consolidation_surcharge.gamma_dry
        self.surcharge_c_v = consolidation_surcharge.c_v
        self.surcharge_drainage_factor = consolidation_surcharge.drainage_factor
        self.surcharge_pop_ocr = consolidation_surcharge.pop_ocr

    def _surcharge_base_consolidation(self, nlayer_old, nlayer_new):
        self.pore_pressure = self._surcharge_array(
            self.pore_pressure, 0.0, nlayer_old, nlayer_new
        )
        self.total_stress = self._surcharge_array(
            self.total_stress, 0.0, nlayer_old, nlayer_new
        )
        self.effective_stress = self._surcharge_array(
            self.effective_stress, 0.0, nlayer_old, nlayer_new
        )
        self.previous_effective_stress = self._surcharge_array(
            self.previous_effective_stress, 0.0, nlayer_old, nlayer_new
        )
        self.preconsolidation_stress = self._surcharge_array(
            self.preconsolidation_stress, 0.0, nlayer_old, nlayer_new
        )
        self.delta_U = self._surcharge_array(self.delta_U, 0.0, nlayer_old, nlayer_new)
        self.strain = self._surcharge_array(self.strain, 0.0, nlayer_old, nlayer_new)
        self.gamma_wet = self._surcharge_array(
            self.gamma_wet, self.surcharge_gamma_wet, nlayer_old, nlayer_new
        )
        self.gamma_dry = self._surcharge_array(
            self.gamma_dry, self.surcharge_gamma_dry, nlayer_old, nlayer_new
        )
        self.c_v = self._surcharge_array(
            self.c_v, self.surcharge_c_v, nlayer_old, nlayer_new
        )
        self.drainage_factor = self._surcharge_array(
            self.drainage_factor, self.surcharge_drainage_factor, nlayer_old, nlayer_new
        )
        self.pop_ocr = self._surcharge_array(
            self.pop_ocr, self.surcharge_pop_ocr, nlayer_old, nlayer_new
        )
        # Update only the new part of the column
        self.f_pore_pressure()
        self.f_total_stress()
        self.f_effective_stress()
        self.previous_effective_stress[nlayer_old:] = self.effective_stress[
            nlayer_old:nlayer_new
        ]

    def _split_base_consolidation(self, index, ratio, nlayer):
        # Enlarge arrays
        self.pore_pressure = np.zeros(nlayer)
        self.total_stress = np.zeros(nlayer)
        self.effective_stress = np.zeros(nlayer)
        self.previous_effective_stress = np.zeros(nlayer)
        self.preconsolidation_stress = np.zeros(nlayer)
        self.delta_U = np.zeros(nlayer)
        self.strain = np.zeros(nlayer)
        # Split states
        self.gamma_wet = self._split_array(self.gamma_wet, index, False, ratio)
        self.gamma_dry = self._split_array(self.gamma_dry, index, False, ratio)
        self.c_v = self._split_array(self.c_v, index, False, ratio)
        self.drainage_factor = self._split_array(
            self.drainage_factor, index, False, ratio
        )
        self.pop_ocr = self._split_array(self.pop_ocr, index, False, ratio)

    def f_timestep_stress(self):
        self.f_total_stress()
        self.f_pore_pressure()
        self.f_effective_stress()

    def f_period_stress(self):
        self.f_timestep_stress()
        self.f_preconsolidation_stress()
        self.previous_effective_stress[:] = self.effective_stress[:]

    def f_pore_pressure(self):
        """
        Pore pressure is linearly interpolated between phreatic_level and
        aquifer head. If the phreatic_level drops below the aquifer_top, the
        aquifer_top is assigned.

        TODO:
        There's still the question of what happens when pore pressure exceeds
        soil weight. The soil will fail, hydraulically, and a boil will form,
        which a) raises the phreatic_level and/or b) lowers the aquifer head.
        """
        if self.aquifer_head == self.phreatic_level:
            hydraulic_head = np.full_like(self.z, self.phreatic_level)
        elif self.phreatic_level <= self.aquifer_top:
            hydraulic_head = np.full_like(self.z, self.aquifer_top)
        else:  # linearly interpolate
            hydraulic_head = self.aquifer_head + (self.z - self.aquifer_top) * (
                self.phreatic_level - self.aquifer_head
            ) / (self.phreatic_level - self.aquifer_top)

            hydraulic_head[self.z < self.aquifer_top] = self.aquifer_head

        pressure_head = hydraulic_head - self.z
        pressure_head[pressure_head < 0.0] = 0.0
        self.pore_pressure = pressure_head * self.gamma_water

    def f_total_stress(self):
        # Take into account whether there's water on top
        difference = self.phreatic_level - self.surface_level
        if difference > 0.0:
            weight_water = self.gamma_water * difference
        else:
            weight_water = 0.0

        cumulative_weight = weight_water
        nlayer = self.total_stress.size
        # Go in reverse order: weight increases top to bottom
        for i in range(nlayer - 1, -1, -1):
            z = self.z[i]  # midpoint of cell
            dz = self.thickness[i]
            ztop = z + 0.5 * dz
            zbot = z - 0.5 * dz
            # cell is fully dry
            if zbot >= self.phreatic_level:
                weight = dz * self.gamma_dry[i]
                midweight = 0.5 * weight
            # cell is fully wet
            elif ztop <= self.phreatic_level:
                weight = dz * self.gamma_wet[i]
                midweight = 0.5 * weight
            # cell is partially wet, partially dry
            else:
                dry_dz = ztop - self.phreatic_level
                wet_dz = self.phreatic_level - zbot
                weight = wet_dz * self.gamma_wet[i] + dry_dz * self.gamma_dry[i]
                # the part above the cell mid is dry
                if self.phreatic_level <= z:
                    midweight = 0.5 * dz * self.gamma_dry[i]
                # the part above the cell mid is partially wet
                else:  # phreatic level > z
                    # dry_dz does not have to be recomputed
                    wet_dz = self.phreatic_level - z
                    midweight = dry_dz * self.gamma_dry[i] + wet_dz * self.gamma_wet[i]

            self.total_stress[i] = cumulative_weight + midweight
            cumulative_weight += weight

    def f_effective_stress(self):
        self.effective_stress = self.total_stress - self.pore_pressure

    def f_preconsolidation_stress(self):
        if self.pop_flag:
            self.preconsolidation_stress = self.effective_stress + self.pop_ocr
        else:
            self.preconsolidation_stress = self.effective_stress * self.pop_ocr

    def f_update_gamma(self):
        # Update gamma for wet part by assuming water is pushed out (gamma_water)
        self.gamma_wet = (
            self.gamma_wet * self.thickness - self.consolidation * self.gamma_water
        ) / (self.thickness - self.consolidation)
        self.gamma_wet[np.isnan(self.gamma_wet) | (self.gamma_wet < 0.0)] = 0.0

        # Update gamma for dry part by assuming air is pushed out (no weight)
        self.gamma_dry = (self.gamma_dry * self.thickness) / (
            self.thickness - self.consolidation
        )
        self.gamma_dry[np.isnan(self.gamma_dry) | (self.gamma_dry < 0.0)] = 0.0

    def f_U(self, time):
        time_factor = (self.c_v * time) / (
            self.initial_thickness * self.drainage_factor
        ) ** 2
        return ((time_factor**3) / (time_factor**3 + 0.5)) ** (1 / 6)

    def f_delta_U(self):
        self.delta_U = self.f_U(self.time) - self.f_U(self.previous_time)
