from typing import NamedTuple

import numpy as np
import numba
from numba.experimental import jitclass
from numba import int16, int8, int64, float64, bool_, int32

from .column_base import discretize
from .consolidation_base import (
    ConsolidationBase,
    ConsolidationInput,
    ConsolidationSurcharge,
    consolidation_empty_surcharge,
)


class IsotacheInput(NamedTuple):
    gamma_wet: np.ndarray
    gamma_dry: np.ndarray
    c_v: np.ndarray
    drainage_factor: np.ndarray
    pop_ocr: np.ndarray
    pop_flag: bool
    aquifer_top: float
    aquifer_head: float
    a: np.ndarray
    b: np.ndarray
    c: np.ndarray


assert set(IsotacheInput._fields).issuperset(ConsolidationInput._fields)


class IsotacheSurcharge(NamedTuple):
    gamma_wet: np.ndarray
    gamma_dry: np.ndarray
    c_v: np.ndarray
    drainage_factor: np.ndarray
    pop_ocr: np.ndarray
    a: np.ndarray
    b: np.ndarray
    c: np.ndarray


assert set(IsotacheSurcharge._fields).issuperset(ConsolidationSurcharge._fields)


isotache_empty_surcharge = IsotacheSurcharge(
    **consolidation_empty_surcharge._asdict(),
    a=np.array([[0.0]]),
    b=np.array([[0.0]]),
    c=np.array([[0.0]]),
)

isotache_spec = [
    ("a", float64[:]),
    ("b", float64[:]),
    ("c", float64[:]),
    ("tau", float64[:]),
    ("tau_intermediate", float64[:]),
    ("surcharge_a", float64[:]),
    ("surcharge_b", float64[:]),
    ("surcharge_c", float64[:]),
]


class IsotacheConsolidation(ConsolidationBase):
    """
    Attributes
    ----------
    a, b, c : np.array of floats
        a, b, c parameters of abc-isotache method
    """

    def _initialize_consolidation(self, consolidation_input):
        self._initialize_base_consolidation(consolidation_input)
        self.a = consolidation_input.a
        self.b = consolidation_input.b
        self.c = consolidation_input.c
        self.f_intitial_intrinsic_time()

    def _set_consolidation_surcharge(
        self, consolidation_surcharge=isotache_empty_surcharge
    ):
        self._set_base_consolidation_surcharge(consolidation_surcharge)
        self.surcharge_a = consolidation_surcharge.a
        self.surcharge_b = consolidation_surcharge.b
        self.surcharge_c = consolidation_surcharge.c

    def _surcharge_consolidation(self, nlayer_old, nlayer_new):
        self._surcharge_base_consolidation(nlayer_old, nlayer_new)
        a = self.surcharge_a
        b = self.surcharge_b
        c = self.surcharge_c
        self.a = self._surcharge_array(self.a, a, nlayer_old, nlayer_new)
        self.b = self._surcharge_array(self.b, b, nlayer_old, nlayer_new)
        self.c = self._surcharge_array(self.c, c, nlayer_old, nlayer_new)

        # Re-initialize intrinsic_time, only for new voxels
        # Use intermediate tau to temporarily store values
        tmp = self.tau.copy()
        self.tau = self._surcharge_array(self.tau, 0.0, nlayer_old, nlayer_new)
        self.tau_intermediate = self._surcharge_array(
            self.tau_intermediate, 0.0, nlayer_old, nlayer_new
        )
        self.f_intitial_intrinsic_time()
        # Overwrite part that wasn't supposed to be replaced
        self.tau[:nlayer_old] = tmp

    def _split_consolidation(self, index, ratio, nlayer):
        self._split_base_consolidation(index, ratio, nlayer)
        self.a = self._split_array(self.a, index, False, ratio)
        self.b = self._split_array(self.b, index, False, ratio)
        self.c = self._split_array(self.c, index, False, ratio)
        self.tau = self._split_array(self.tau, index, False, ratio)
        self.tau_intermediate = self._split_array(
            self.tau_intermediate, index, False, ratio
        )

    def isotache_check(self):
        size = self.thickness.size
        self.check_positive(self.a)
        self.check_positive(self.b)
        self.check_positive(self.c)

    def f_intitial_intrinsic_time(self):
        """
        Compute intrinsic time (tau)
        """
        nlayer = self.thickness.size
        self.tau = np.zeros(nlayer)
        self.tau_intermediate = np.zeros(nlayer)
        self.f_pore_pressure()
        self.f_total_stress()
        self.f_effective_stress()
        if self.pop_flag:  # Overburden pressure
            ocr = (self.effective_stress + self.pop_ocr * 1.0e3) / self.effective_stress
        else:  # Overconsolidation ratio
            ocr = self.pop_ocr

        reference_tau = 1.0
        for i in range(nlayer):
            if self.c[i] < 1.0e-4:
                self.tau[i] = 1.0e9
            else:
                abcterm = (self.b[i] - self.a[i]) / self.c[i]
                self.tau[i] = reference_tau * ocr[i] ** abcterm

    def f_consolidation(self):
        """
        calculate consolidation by loading using abc-isotache method.
        """
        self.consolidation[:] = 0.0
        nlayer = self.thickness.size
        time = self.time
        previous_time = self.previous_time

        timestep = time - previous_time
        delta_effective_stress = self.effective_stress - self.previous_effective_stress
        actual_effective_stress = (
            self.previous_effective_stress
            + self.f_U(self.time) * delta_effective_stress
        )
        self.f_delta_U()
        loadstep = self.delta_U * delta_effective_stress

        for i in range(nlayer):
            if self.c[i] < 1.0e-4:
                self.tau_intermediate[i] = self.tau[i]
            else:
                abcterm = (self.b[i] - self.a[i]) / self.c[i]
                stress_term = (
                    actual_effective_stress[i] - loadstep[i]
                ) / actual_effective_stress[i]
                self.tau_intermediate[i] = self.tau[i] * stress_term**abcterm
        # update tau
        self.tau[:] = self.tau_intermediate + timestep
        self.strain = self.a * np.log(
            actual_effective_stress / (actual_effective_stress - loadstep)
        ) + self.c * np.log(self.tau / self.tau_intermediate)

        for i in range(nlayer):
            dz = self.thickness[i]
            if dz == 0.0:
                continue
            if actual_effective_stress[i] == 0.0:
                continue
            # TODO: check nan
            self.consolidation[i] = min(dz, self.strain[i] * self.thickness[i])

    # Would've preferably been @staticmethod, but not supported by numba
    def select_consolidation_input(self, consolidation_input, nlayer, sublayers, index):
        return IsotacheInput(
            gamma_wet=discretize(
                consolidation_input.gamma_wet[index], nlayer, sublayers
            ),
            gamma_dry=discretize(
                consolidation_input.gamma_dry[index], nlayer, sublayers
            ),
            a=discretize(consolidation_input.a[index], nlayer, sublayers),
            b=discretize(consolidation_input.b[index], nlayer, sublayers),
            c=discretize(consolidation_input.c[index], nlayer, sublayers),
            drainage_factor=discretize(
                consolidation_input.drainage_factor[index], nlayer, sublayers
            ),
            c_v=discretize(consolidation_input.c_v[index], nlayer, sublayers),
            pop_ocr=discretize(consolidation_input.pop_ocr[index], nlayer, sublayers),
            aquifer_top=consolidation_input.aquifer_top[index],
            aquifer_head=consolidation_input.aquifer_head[index],
            pop_flag=consolidation_input.pop_flag,
        )

    def select_consolidation_surcharge(
        self, consolidation_surcharge, nlayer, sublayers, period, index
    ):

        return IsotacheSurcharge(
            gamma_wet=discretize(
                consolidation_surcharge.gamma_wet[period, index], nlayer, sublayers
            ),
            gamma_dry=discretize(
                consolidation_surcharge.gamma_dry[period, index], nlayer, sublayers
            ),
            a=discretize(consolidation_surcharge.a[period, index], nlayer, sublayers),
            b=discretize(consolidation_surcharge.b[period, index], nlayer, sublayers),
            c=discretize(consolidation_surcharge.c[period, index], nlayer, sublayers),
            c_v=discretize(
                consolidation_surcharge.c_v[period, index], nlayer, sublayers
            ),
            drainage_factor=discretize(
                consolidation_surcharge.drainage_factor[period, index],
                nlayer,
                sublayers,
            ),
            pop_ocr=discretize(
                consolidation_surcharge.pop_ocr[period, index], nlayer, sublayers
            ),
        )
