"""
This is the second class of the subsidence calculations

A control unit holds the data of multiple soil columns. It's also
responsible for setting up these soil columns, and calling the methods

"""
import dask
import numba
import numpy as np
import xarray as xr

import atlans
from atlans.column import ColumnInput
from atlans.column import INPUTS
from atlans.column import SURCHARGES
from atlans.column import EMPTY_SURCHARGES as empty_surcharges
from atlans.column import ColumnSurcharge
from atlans.column import create_soil_column_class


# The empty surcharges are to aid enable type inferencing of numba
# For the ControlUnit, the empty surcharges have to be three dimensional
# since it will index [period, column_index], and this has to result in
# an array.
EMPTY_SURCHARGES = {}
for key, tup in empty_surcharges.items():
    args = tuple(np.atleast_3d(e) for e in tup)
    EMPTY_SURCHARGES[key] = type(tup)(*args)


@numba.njit
def na_index(array):
    """
    Returns the index of the first Not-a-Number value.
    Returns length of array in case no NaN's are found,
    so it can always be used for indexing.
    """
    nlayer = array.shape[0]
    for i in range(nlayer):
        if np.isnan(array[i]):
            return i
    return nlayer


@numba.njit
def discretize_thickness(thickness, maximum_thickness):
    """
    Enlarges array based on thickness (array) and maximum allowable
    thickness thereby creating more layers.
    Divides the parameter by the elementwise multiplication if divsion
    is True; e.g. when a thickness cell is changed into three cells,
    the resulting three cells have a thickness * 1/3.

    Returns
    -------
    np.array
        Rediscretized array
    """
    # Cut off nan part
    valid = na_index(thickness)
    thickness = thickness[:valid]
    nlayer = thickness.size
    sublayers = np.ceil(thickness / maximum_thickness).astype(np.int32)
    new_thickness = np.full(sublayers[sublayers > 0].sum(), np.nan)

    count = 0
    for i in range(nlayer):
        n_elem = sublayers[i]
        if n_elem > 0:
            new_thickness[count : count + n_elem] = thickness[i] / n_elem
            count += n_elem
    new_nlayer = new_thickness.size
    return sublayers, new_thickness, new_nlayer


@numba.njit
def find_period_input(forcing_periods, period):
    for i, p in enumerate(forcing_periods):
        if period == p:
            return i
    return -1


class ControlUnit:
    """
    Constructs the input on which the numba functions can operate.

    Parameters
    ----------

    options : atlans.datatype
        custom datatype, four fields:

        * nperiods: np.int64
        * maximum_thickness: np.float64
        * pop_flag: np.bool
        * track_subsidence: np.bool

    Returns
    -------
    subsidence : np.array
        shape: `(nrows, ncols, nperiods)`
    oxidation : np.array
        shape: `(nrows, ncols, nperiods, nlayer)`
    thickness : np.array
        shape: `(nrows, ncols, nperiods, nlayer)`
    """

    # TODO: we might be able to use typed lists with a next numba release.

    def __init__(self, dataset, parameters, options):
        self.dataset = dataset.transpose("column", "layer")
        self.parameters = parameters
        self.options = options
        self.first_call = True

    def reinit(self, dataset, parameters, options):
        self.dataset = dataset.transpose("column", "layer")
        self.parameters = parameters
        self.options = options

    def _make_create_columns(self):
        """
        Factory function to create a function that creates isotache or koppejan
        columns.

        Alternatively, two functions could be created, but this minimizes code
        duplication between isotache and koppejan case, which still share a great
        of functionality.

        Use a closure to avoid numba function argument overhead for performance
        reasons:
        https://numba.pydata.org/numba-doc/dev/user/faq.html#can-i-pass-a-function-as-an-argument-to-a-jitted-function

        This might seem unnecessarily complicated, but this is as far as I can
        tell, the best way to get the desired dynamic behavior at runtime
        without significant performance loss or code duplication.

        Parameters
        ----------
        consolidation : str, {"koppejan", "isotache"}

        Returns
        -------
        create_columns : function
        """
        # Module is inserted at compile time, no overhead
        @numba.njit
        def create_soilcolumns(
            column_input,
            consolidation_input,
            oxidation_input,
            maximum_thickness,
            has_data,
            SoilColumn,
            InputSelector,
        ):
            # TODO: Take typedlist into account in due time?
            soil_columns = []
            thickness = column_input.thickness
            ncolumns = len(thickness)

            # jitclass doesn't support static methods
            # gotta use this workaround instead ...
            static_column = InputSelector()

            for i in range(ncolumns):
                if not has_data[i]:
                    continue
                # Create specific input tuple
                # Discretize according to allowed maximum thickness
                sublayers, new_thickness, nlayer = discretize_thickness(
                    thickness[i], maximum_thickness
                )
                this_column_input = static_column.select_column_input(
                    column_input, new_thickness, i
                )
                this_consolidation_input = static_column.select_consolidation_input(
                    consolidation_input, nlayer, sublayers, i
                )
                this_oxidation_input = static_column.select_oxidation_input(
                    oxidation_input, nlayer, sublayers, i
                )
                # Create column and add to list
                soil_columns.append(
                    SoilColumn(
                        this_column_input,
                        this_consolidation_input,
                        this_oxidation_input,
                    )
                )
            return soil_columns

        SoilColumn = create_soil_column_class(
            type(self.consolidation_input),
            type(self.oxidation_input),
        )
        InputSelector = atlans.column.create_input_selector(
            type(self.consolidation_input),
            type(self.oxidation_input),
        )

        # Get rid of runtime overhead by resolving SoilColumn type at compile time instead
        # Use of a closure like this is a typical solution in numba
        @numba.njit
        def create_columns(
            column_input,
            consolidation_input,
            oxidation_input,
            maximum_thickness,
            has_data,
        ):
            return create_soilcolumns(
                column_input,
                consolidation_input,
                oxidation_input,
                maximum_thickness,
                has_data,
                SoilColumn,
                InputSelector,
            )

        self.create_columns = create_columns

    @staticmethod
    @numba.njit
    def _build_lookup_table(lithology, geology, values):
        """
        Takes three arrays, read in from a .tsv file giving parameter
        information.

        Returns an array with N x M elements, where N is the maximum lithology
        code, and M the maximum geology code, as integers.

        The returned array is (much) larger than strictly necessary (as lithology numbering
        starts in the thousands, and is not sequential). However, numpy indexing is quick,
        and this method is conceptually very simple as it requires no further modification
        of geology codes.

        A workaround for the lack of dictionary support in numba.
        See also: https://github.com/numba/numba/issues/2096
        """
        assert lithology.shape == geology.shape == values.shape
        nclasses = lithology.size
        nrow = int(geology.max()) + 1
        ncol = int(lithology.max()) + 1
        array = np.full((nrow, ncol), np.nan)
        for n in range(nclasses):
            i = lithology[n]
            j = geology[n]
            array[j, i] = values[n]
        return array

    @staticmethod
    @numba.njit
    def _fill(size, geology, lithology, lookup_table):
        new_array = np.full(size, np.nan)
        for i in range(size):
            ii = geology[i]
            jj = lithology[i]
            if (ii < 0) or (jj < 0):  # nodata
                continue
            new_array[i] = lookup_table[ii, jj]
        return new_array

    def fill_by_lookup(self, dataset, field):
        shape = dataset["thickness"].shape
        size = dataset["thickness"].size
        lithology = dataset["lithology"].values.reshape(size)
        geology = dataset["geology"].values.reshape(size)
        # Create lookup table from parameters attribute
        values = self.parameters[field]
        lithology_lookup = self.parameters["lithology"]
        geology_lookup = self.parameters["geology"]
        lookup_table = self._build_lookup_table(
            lithology_lookup, geology_lookup, values
        )
        out = self._fill(size, geology, lithology, lookup_table)
        return out.reshape(shape)

    def dataset_to_namedtuple(self):
        """
        Go from parameters and dataset to a full set of arrays of model input
        This can later be checked to see if there's sufficient room for surcharge layers.
        """
        ConsolidationInput = INPUTS[self.options["consolidation"]]
        OxidationInput = INPUTS[self.options["oxidation"]]

        column_kwargs = {}
        for field in ColumnInput._fields:
            if field in self.dataset:
                column_kwargs[field] = self.dataset[field].values
            else:
                column_kwargs[field] = self.fill_by_lookup(self.dataset, field)

        consolidation_kwargs = {"pop_flag": self.options["pop_flag"]}
        for field in ConsolidationInput._fields:
            if field in ("sublayers", "pop_flag"):
                continue
            elif field in self.dataset:
                consolidation_kwargs[field] = self.dataset[field].values
            else:  # if it's not in dataset, it's lookup data
                consolidation_kwargs[field] = self.fill_by_lookup(self.dataset, field)

        oxidation_kwargs = {}
        for field in OxidationInput._fields:
            if field in self.dataset:
                oxidation_kwargs[field] = self.dataset[field].values
            else:
                oxidation_kwargs[field] = self.fill_by_lookup(self.dataset, field)

        # Parameterized construction
        self.column_input = ColumnInput(**column_kwargs)
        self.consolidation_input = ConsolidationInput(**consolidation_kwargs)
        self.oxidation_input = OxidationInput(**oxidation_kwargs)

    def surcharge_to_namedtuple(self, surcharge):
        # For stable typing
        if surcharge is None:
            surcharge_column = EMPTY_SURCHARGES["column"]
            surcharge_consolidation = EMPTY_SURCHARGES[self.options["consolidation"]]
            surcharge_oxidation = EMPTY_SURCHARGES[self.options["oxidation"]]
            surcharge_periods = np.array([], dtype=np.int)
        else:
            ConsolidationSurcharge = SURCHARGES[self.options["consolidation"]]
            OxidationSurcharge = SURCHARGES[self.options["oxidation"]]

            surcharge_periods = surcharge.coords["period"].values

            consolidation_kwargs = {}
            for field in ConsolidationSurcharge._fields:
                if field in surcharge:
                    consolidation_kwargs[field] = surcharge[field].values
                else:  # if it's not in dataset, it's lookup data
                    consolidation_kwargs[field] = self.fill_by_lookup(surcharge, field)

            oxidation_kwargs = {}
            for field in OxidationSurcharge._fields:
                if field in surcharge:
                    oxidation_kwargs[field] = surcharge[field].values
                else:
                    oxidation_kwargs[field] = self.fill_by_lookup(surcharge, field)

            surcharge_column = ColumnSurcharge(surcharge["thickness"].values)
            surcharge_consolidation = ConsolidationSurcharge(**consolidation_kwargs)
            surcharge_oxidation = OxidationSurcharge(**oxidation_kwargs)
        return (
            surcharge_column,
            surcharge_consolidation,
            surcharge_oxidation,
            surcharge_periods,
        )

    def lowering_to_tuple(self, lowering):
        if lowering is None:
            lowering_periods = np.array([], dtype=np.int)
            tuple_lowering = atlans.WaterTableLowering(
                phreatic_lowering=np.array([[0.0]]),
                aquifer_lowering=np.array([[0.0]]),
                track_subsidence=np.array([[False]]),
            )
            return tuple_lowering, lowering_periods
        else:
            ncolumns = self.dataset["thickness"].shape[0]
            if tuple(lowering.dims) != ("column", "period"):
                raise ValueError(
                    'Dimensions of lowering must be exactly "column" and "period"'
                )
            if lowering.coords["column"].size != ncolumns:
                raise ValueError(
                    "Number of value in lowering does not match number of columns in"
                    " control unit"
                )
            tuple_lowering = atlans.WaterTableLowering(
                phreatic_lowering=lowering["phreatic_lowering"].values,
                aquifer_lowering=lowering["aquifer_lowering"].values,
                track_subsidence=lowering["track_subsidence"].values,
            )
            lowering_periods = lowering.coords["period"].values
            return tuple_lowering, lowering_periods

    def _columns_with_data(self):
        ncolumns, _ = self.dataset["thickness"].shape
        has_data = np.full(ncolumns, True)
        for key, da in self.dataset.data_vars.items():

            if "layer" in da.dims:
                da_hasdata = da.notnull().any("layer").values
                has_data = has_data & da_hasdata
            elif "period" in da.dims:
                da_hasdata = da.notnull().any("period").values
                has_data = has_data & da_hasdata
            else:
                da_hasdata = da.notnull().values
                has_data = has_data & da_hasdata

            if len(has_data.shape) > 1:
                raise ValueError(f"Wrong dimension on input: {key}")
        self._has_data = has_data

    @staticmethod
    @numba.njit
    def _run_stressperiods(
        column_input,
        consolidation_input,
        oxidation_input,
        # temporal discretization
        period_timesteps,
        # stresses
        lowering_periods,
        lowering,
        surcharge_periods,
        surcharge_column,
        surcharge_consolidation,
        surcharge_oxidation,
        # jit'ed function
        create_soilcolumns,
        maximum_thickness,
        # boolean for data/nodata
        has_data,
        # Pre-allocated output arrays
        result_subsidence,
        result_consolidation,
        result_oxidation,
    ):
        """
        No need for the closure business here:
        the overhead of create_soilcolumns is incurred only once.

        Has to be a staticmethod, since numba doesn't understand self.

        Mutates:
            * result_subsidence
            * result_consolidation
            * result_oxidation
        """
        # First: setup all the columns
        soil_columns = create_soilcolumns(
            column_input,
            consolidation_input,
            oxidation_input,
            maximum_thickness,
            has_data,
        )

        # Now run the stress periods
        ncolumn = len(soil_columns)
        nperiod = len(period_timesteps)

        track_subsidence = np.full(ncolumn, False)
        track_subsidence_lowering = 0.0
        for t in range(nperiod):
            # If there's no data present at all, the column is absent
            # So we need two counter: one for the data (including nodata)
            # and one for the actually present columns
            count = 0
            for i in range(ncolumn):
                # Don't simulate if there's no data
                if not has_data[i]:
                    continue

                column = soil_columns[count]

                phreatic_level_lowering = 0.0
                aquifer_level_lowering = 0.0
                lowering_period = find_period_input(lowering_periods, t)
                if lowering_period != -1:
                    phreatic_level_lowering = lowering.phreatic_lowering[t, i]
                    aquifer_level_lowering = lowering.aquifer_lowering[t, i]
                    track_subsidence[i] = lowering.track_subsidence[t, i]
                if track_subsidence[i]:
                    phreatic_level_lowering += track_subsidence_lowering
                    aquifer_level_lowering += track_subsidence_lowering
                column.phreatic_lowering = phreatic_level_lowering
                column.aquifer_lowering = aquifer_level_lowering

                surcharge_period = find_period_input(surcharge_periods, t)
                if surcharge_period != -1:
                    thickness = surcharge_column.thickness
                    sublayers, new_thickness, nlayer = discretize_thickness(
                        thickness[surcharge_period, i], maximum_thickness
                    )
                    this_column_surcharge = ColumnSurcharge(new_thickness)
                    this_consolidation_surcharge = (
                        column.select_consolidation_surcharge(
                            surcharge_consolidation,
                            nlayer,
                            sublayers,
                            surcharge_period,
                            i,
                        )
                    )
                    this_oxidation_surcharge = column.select_oxidation_surcharge(
                        surcharge_oxidation,
                        nlayer,
                        sublayers,
                        surcharge_period,
                        i,
                    )
                    column.set_surcharge(
                        this_column_surcharge,
                        this_consolidation_surcharge,
                        this_oxidation_surcharge,
                    )

                # t for transient
                (
                    consolidation_t,
                    oxidation_t,
                    subsidence_t,
                ) = column.advance_stressperiod(timesteps=period_timesteps[t])
                result_subsidence[i, t] = subsidence_t
                result_consolidation[i, t] = consolidation_t
                result_oxidation[i, t] = oxidation_t

                count += 1
                # end column loop

            # Get the median lowering
            track_subsidence_lowering = np.nanpercentile(result_subsidence[:, t], 50)
            # end period loop

    def run_stressperiods(
        self,
        period_timesteps,
        lowering=None,
        surcharge=None,
    ):
        """
        Parameters
        ----------
        period_timestep : list of numpy arrays
        lowering : xarray.Dataset
            with variables {"lowering", "track_subsidence"}
        surcharge : xarray.Dataset
            Not implemented
        """
        nperiods = len(period_timesteps)
        ncolumns = self.dataset.coords["column"].size
        nlayer = self.dataset.coords["layer"].size

        # Initialize input in namedtuple form (which is understood by numba)
        # Prepare modelinput fully, go from tables to filled arrays
        self.dataset_to_namedtuple()

        # Make the create_columns function
        # Only the first time
        self._columns_with_data()
        if self.first_call:
            self._make_create_columns()

        # Pre-allocate output arrays so allocation does not happen continuously
        # during computation
        subsidence = np.full((ncolumns, nperiods), np.nan)
        consolidation = np.full((ncolumns, nperiods), np.nan)
        oxidation = np.full((ncolumns, nperiods), np.nan)

        lowering_tuple, lowering_periods = self.lowering_to_tuple(lowering)
        (
            surcharge_column,
            surcharge_consolidation,
            surcharge_oxidation,
            surcharge_periods,
        ) = self.surcharge_to_namedtuple(surcharge)

        # Run all columns in one go
        self._run_stressperiods(
            # input data
            column_input=self.column_input,
            consolidation_input=self.consolidation_input,
            oxidation_input=self.oxidation_input,
            # transient data
            period_timesteps=period_timesteps,
            # stresses
            lowering_periods=lowering_periods,
            lowering=lowering_tuple,
            surcharge_periods=surcharge_periods,
            surcharge_column=surcharge_column,
            surcharge_consolidation=surcharge_consolidation,
            surcharge_oxidation=surcharge_oxidation,
            # column constructor
            create_soilcolumns=self.create_columns,
            maximum_thickness=self.options["maximum_thickness"],
            # boolean for data/nodata columns
            has_data=self._has_data,
            # output arrays
            result_subsidence=subsidence,
            result_consolidation=consolidation,
            result_oxidation=oxidation,
        )
        self.first_call = False

        self.subsidence = subsidence
        self.consolidation = consolidation
        self.oxidation = oxidation

    def output(self):
        coords = dict(self.dataset.coords)
        coords.pop("layer")
        dims = ("column", "period")
        output = xr.Dataset()

        output["subsidence"] = xr.DataArray(
            self.subsidence,
            coords,
            dims,
        )
        output["consolidation"] = xr.DataArray(
            self.consolidation,
            coords,
            dims,
        )
        output["oxidation"] = xr.DataArray(
            self.oxidation,
            coords,
            dims,
        )
        return output
