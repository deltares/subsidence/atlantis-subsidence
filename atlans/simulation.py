import copy
import collections
import logging
import pathlib

import dask
from numba.typed import List
import numpy as np
import pandas as pd
import xarray as xr
from dask.diagnostics import ProgressBar

import atlans


def validate_path(path):
    if isinstance(path, str):
        path = pathlib.Path(path)
    if not path.exists():
        raise FileNotFoundError(f"{path}")
    return path


def compilation_run(
    controlunit_dataset, lowering, surcharge, parameters, options, period_timesteps
):
    controlunit_dataset["thickness"] = controlunit_dataset["thickness"].where(
        controlunit_dataset["thickness"] > 0.0
    )
    control_unit = atlans.ControlUnit(
        controlunit_dataset, parameters=parameters, options=options
    )
    lowering_input = (
        lowering.transpose("period", "column") if lowering is not None else None
    )
    surcharge_input = (
        surcharge.transpose("period", "column", "layer")
        if surcharge is not None
        else None
    )
    control_unit.run_stressperiods(period_timesteps, lowering_input, surcharge_input)
    return control_unit


def groupby_indices(control_unit):
    d_y = collections.defaultdict(list)
    d_x = collections.defaultdict(list)
    count = collections.defaultdict(int)

    nrow, ncol = control_unit.shape
    for i in range(nrow):
        for j in range(ncol):
            c = control_unit[i, j]
            if c == -1:
                continue
            d_y[c].append(i)
            d_x[c].append(j)
            count[c] += 1

    d_y = {
        k: xr.DataArray(v, {"column": np.arange(len(v))}, ["column"])
        for k, v in d_y.items()
    }
    d_x = {
        k: xr.DataArray(v, {"column": np.arange(len(v))}, ["column"])
        for k, v in d_x.items()
    }

    return d_y, d_x, count


def simulate_controlunit(
    controlunit_dataset,
    lowering,
    surcharge,
    control_unit,
    parameters,
    options,
    period_timesteps,
):
    parameters = parameters.copy()

    controlunit_dataset["thickness"] = controlunit_dataset["thickness"].where(
        controlunit_dataset["thickness"] > 0.0
    )
    lowering_input = (
        lowering.transpose("period", "column") if lowering is not None else None
    )
    surcharge_input = (
        surcharge.transpose("period", "column", "layer")
        if surcharge is not None
        else None
    )

    control_unit = copy.copy(control_unit)
    control_unit.reinit(controlunit_dataset, parameters, options)
    control_unit.run_stressperiods(period_timesteps, lowering_input, surcharge_input)
    output = control_unit.output()
    return output


class Simulation:
    def __init__(
        self,
        name,
        parameters,
        dataset,
        consolidation,
        oxidation,
        preconsolidation_stress,
        maximum_thickness,
        xmin=None,
        xmax=None,
        ymin=None,
        ymax=None,
    ):
        self.name = name
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        self.path_parameters = validate_path(parameters)
        if isinstance(dataset, str):
            self.path_dataset = validate_path(dataset)
        else:
            self.path_dataset = dataset
        self.preconsolidation_stress = preconsolidation_stress

        self.options = {}
        if self.preconsolidation_stress == "pop":
            self.options["pop_flag"] = True
        elif self.preconsolidation_stress == "ocr":
            self.options["pop_flag"] = False
        else:
            raise ValueError("Invalid preconsolidation_stress setting")
        self.options["maximum_thickness"] = maximum_thickness
        self.options["consolidation"] = consolidation
        self.options["oxidation"] = oxidation
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)

    @property
    def preconsolidation_stress(self):
        return self._preconsolidation_stress

    @preconsolidation_stress.setter
    def preconsolidation_stress(self, value):
        v = value.lower()
        if v not in ("pop", "ocr"):
            raise ValueError(
                'Invalid preconsolidation_stress value. Valid values are: "pop", "ocr".'
            )
        self._preconsolidation_stress = v

    @property
    def consolidation(self):
        return self._consolidation

    @consolidation.setter
    def consolidation(self, value):
        v = value.lower()
        if v not in ("koppejan", "isotache", None):
            raise ValueError(
                "Invalid preconsolidation_stress value. Valid values are:"
                ' "koppejan", "isotache", None.'
            )
        self._consolidation = v

    @property
    def oxidation(self):
        return self._consolidation

    @consolidation.setter
    def oxidation(self, value):
        v = value.lower()
        if v not in ("constant_rate", "carbon_store", None):
            raise ValueError(
                "Invalid preconsolidation_stress value. Valid values are:"
                ' "constant_rate", "carbon_store", None.'
            )
        self._consolidation = v

    def load_parameters(self):
        """
        Reads a tab-separated values (.tsv) file in which parameters are defined
        for geology layers, and lithologies. These are returned a dict
        of np.arrays for efficient fetching numba functions; as dictionaries are
        not supported by numba.

        The returned array is (much) larger than strictly necessary (as geology numbering
        starts in the thousands, and is not sequential). However, numpy indexing is quick,
        and this method is conceptually very simple as it requires no further modification
        of geology codes.
        """
        suffix = pathlib.Path(self.path_parameters).suffix
        if suffix == ".xlsx":
            df = pd.read_excel(self.path_parameters)
        elif suffix == ".tsv":
            df = pd.read_csv(self.path_parameters, sep="\t")
        else:
            df = pd.read_csv(self.path_parameters)
        self.parameter_dict = {}

        for column in df.columns:
            # split off unit annotation from field name
            dname = column.split(" (")[0]
            values = df[column].values
            self.parameter_dict[dname] = values

    def open_data(self):
        logging.info("Opening data files")
        # Tabular data
        self.load_parameters()
        # Crop to desired size.
        if isinstance(self.path_dataset, pathlib.Path):
            self.dataset = xr.open_dataset(self.path_dataset).sel(
                x=slice(self.xmin, self.xmax), y=slice(self.ymax, self.ymin)
            )
        else:
            self.dataset = self.path_dataset.sel(
                x=slice(self.xmin, self.xmax), y=slice(self.ymax, self.ymin)
            )

    def calculate(self, period_timesteps, lowering=None, surcharge=None):
        # Include stress period forcing here so they are also grouped correctly.
        # Also forces correct alignment of forcings.
        if lowering is not None:
            lowering = lowering.sel(
                x=slice(self.xmin, self.xmax), y=slice(self.ymax, self.ymin)
            )

        if surcharge is not None:
            surcharge = surcharge.sel(
                x=slice(self.xmin, self.xmax), y=slice(self.ymax, self.ymin)
            )

        self.logger.info("Allocating output arrays")
        nrow, ncol = self.dataset["surface_level"].shape
        nperiod = len(period_timesteps)

        coords = dict(self.dataset["surface_level"].coords)
        coords["period"] = np.arange(nperiod)
        dims = ("y", "x", "period")

        self.logger.info("Grouping control areas")
        indices_y, indices_x, cell_count = groupby_indices(
            self.dataset["control_unit"].values
        )

        self.logger.info("Compiling...")
        # Compilation run
        first_key = min(cell_count, key=cell_count.get)
        ind_x = indices_x[first_key]
        ind_y = indices_y[first_key]

        # Change period timesteps to a numba typed list
        typed_timesteps = List()
        for ts in period_timesteps:
            typed_timesteps.append(ts.astype(np.float))

        control_unit = compilation_run(
            controlunit_dataset=self.dataset.isel(y=ind_y, x=ind_x),
            lowering=lowering.isel(y=ind_y, x=ind_x) if lowering is not None else None,
            surcharge=surcharge.isel(y=ind_y, x=ind_x)
            if surcharge is not None
            else None,
            parameters=self.parameter_dict,
            options=self.options,
            period_timesteps=typed_timesteps,
        )

        self.logger.info("Setting up DAG")
        collection = []
        for (k, ind_x), (k, ind_y) in zip(indices_x.items(), indices_y.items()):
            output = dask.delayed(simulate_controlunit)(
                controlunit_dataset=self.dataset.isel(y=ind_y, x=ind_x),
                lowering=lowering.isel(y=ind_y, x=ind_x)
                if lowering is not None
                else None,
                surcharge=surcharge.isel(y=ind_y, x=ind_x)
                if surcharge is not None
                else None,
                control_unit=control_unit,
                parameters=self.parameter_dict,
                options=self.options,
                period_timesteps=typed_timesteps,
            )
            collection.append(output)

        self.logger.info("Computing control areas")
        with ProgressBar():
            result = dask.compute(collection)[0]

        self.logger.info("Merging output")
        consolidation = np.full((nrow, ncol, nperiod), np.nan)
        oxidation = np.full((nrow, ncol, nperiod), np.nan)
        subsidence = np.full((nrow, ncol, nperiod), np.nan)
        for ind_x, ind_y, output in zip(indices_x.values(), indices_y.values(), result):
            o = output["oxidation"].values
            c = output["consolidation"].values
            s = output["subsidence"].values
            consolidation[ind_y.values, ind_x.values, :] = c
            oxidation[ind_y.values, ind_x.values, :] = o
            subsidence[ind_y.values, ind_x.values, :] = s

        self.result = xr.Dataset()
        self.result["consolidation"] = xr.DataArray(consolidation, coords, dims)
        self.result["oxidation"] = xr.DataArray(oxidation, coords, dims)
        self.result["subsidence"] = xr.DataArray(subsidence, coords, dims)

    def output(self, filename):
        # This is where actual computation occurs
        self.result.to_netcdf(filename)
