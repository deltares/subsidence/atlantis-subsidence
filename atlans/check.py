import numba
import numpy as np
import xarray as xr


@numba.njit
def check_lithocodes(geology, lithology, given_values):
    """
    Checks whether all occuring geology and lithology codes are
    defined in parameters file.
    """
    nrow, ncol, nlayer = geology.shape
    missing_geology = [geology[0, 0, 0]]
    missing_lithology = [lithology[0, 0, 0]]
    for i in range(nrow):
        for j in range(ncol):
            for k in range(nlayer):
                val_strat = geology[i, j, k]
                val_class = lithology[i, j, k]
                if val_strat == -1:
                    pass
                else:
                    if np.isnan(given_values[val_strat, val_class]):
                        missing_geology = missing_geology.append(val_strat)
                        missing_lithology = missing_lithology.append(val_class)
    return missing_geology, missing_lithology


def check_geometry(ds):
    """
    Checks whether geometry of inputs is consistent:

    * Does surface_level occur above phreatic_level
    * Does aquifer top occur below surface level
    """
    return None


@numba.njit
def valid_column(water_surface, lithology):
    """
    Checks whether column is not water, and that it does fully
    consist of NoData values.
    """
    if not water_surface and not (lithology == -1).all():
        return True
    else:
        return False


def check_column_continuity(geology, lithology):
    """
    Checks whether `nan`'s are located only on top of column (air).
    """
    return None
