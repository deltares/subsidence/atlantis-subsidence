Atlantis Subsidence (atlans): Soil subsidence calculations in Python
====================================================================

Atlans (**Atlan**\tis **S**\ubsidence), is a Python package to calculate soil
(1D) subsidence using voxel based geological lithological models, such as GeoTOP
(Stafleu, ...).

atlans can be used for calculating soil consolidation and peat oxidation.
Currently, only Koppejan's method (Koppejan (...), De Lange (...)) has been
implemented. Methods for oxidation calculations are based on Van den Akker
(...)

atlans makes use of several (scientific) Python packages to handle
larger-than-memory datasets and to achieve reasonable performance without
resorting to (compiled) languages other than Python:

* `xarray`
* `numba`
* `dask`
* `netCDF4`