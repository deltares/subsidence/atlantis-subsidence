import os

os.environ["NUMBA_DISABLE_JIT"] = "1"

import dask
import numba
import numpy as np
import xarray as xr

import atlans
from atlans.column import ColumnInput
from atlans.column import INPUTS
from atlans.column import SURCHARGES
from atlans.column import EMPTY_SURCHARGES
from atlans.column import create_soil_column_class


TIMES = np.array(
    [
        0.0,
        1.0,
        2.0,
        4.0,
        8.0,
        16.0,
        32.0,
        64.0,
        128.0,
        256.0,
        512.0,
        1024.0,
        2048.0,
        3650.0,
    ]
)
TIMESTEPS = np.diff(TIMES)


def control_unit_koppejan():
    """
    Create a control unit of 10 identical columns, like in test_koppejan.
    """
    ncol = 10
    nlayer = 18

    coords2d = {"column": range(ncol), "layer": range(nlayer)}
    dims2d = ("column", "layer")
    coords1d = {"column": range(ncol)}
    dims1d = ("column",)

    dataset = xr.Dataset()
    dataset["thickness"] = xr.DataArray(
        data=np.full((ncol, nlayer), 0.5), coords=coords2d, dims=dims2d
    )
    dataset["lithology"] = xr.DataArray(
        data=np.full((ncol, nlayer), 1, dtype=np.int8), coords=coords2d, dims=dims2d
    )
    dataset["geology"] = xr.DataArray(
        data=np.full((ncol, nlayer), 1, dtype=np.int8), coords=coords2d, dims=dims2d
    )
    dataset["aquifer_head"] = xr.DataArray(
        data=np.full(ncol, -3.0), coords=coords1d, dims=dims1d
    )
    dataset["aquifer_top"] = xr.DataArray(
        data=np.full(ncol, -12.0), coords=coords1d, dims=dims1d
    )
    dataset["surface_level"] = xr.DataArray(
        data=np.full(ncol, -3.0), coords=coords1d, dims=dims1d
    )
    dataset["phreatic_level"] = xr.DataArray(
        data=np.full(ncol, -3.0), coords=coords1d, dims=dims1d
    )
    dataset["zbase"] = xr.DataArray(
        data=np.full(ncol, -12.0), coords=coords1d, dims=dims1d
    )
    dataset["water_surface"] = xr.DataArray(
        data=np.full(ncol, False), coords=coords1d, dims=dims1d
    )
    dataset["maximum_oxidation_depth"] = xr.DataArray(
        data=np.full(ncol, 1.2), coords=coords1d, dims=dims1d
    )

    parameters = {
        "lithology": np.array([1], dtype=np.int8),
        "geology": np.array([1], dtype=np.int8),
        "gamma_wet": np.array([10_500.0]),
        "gamma_dry": np.array([3_500.0]),
        "preCp": np.array([20.0]),
        "preCs": np.array([80.0]),
        "postCs": np.array([5.0]),
        "postCp": np.array([20.0]),
        "c_v": np.array([0.006912]),
        "drainage_factor": np.array([1.0]),
        "pop_ocr": np.array([10_000.0]),
        "oxidation_rate": np.array([4.1095890410958905e-05]),
    }

    options = {
        "pop_flag": True,
        "maximum_thickness": 0.25,
        "consolidation": "koppejan",
        "oxidation": "constant_rate",
    }

    control_unit = atlans.ControlUnit(
        dataset=dataset, parameters=parameters, options=options
    )

    return control_unit


control_unit = control_unit_koppejan()
# repeat twenty times
period_timesteps = np.vstack([TIMESTEPS for _ in range(20)])
coords = {"period": range(20), "column": range(10)}
dims = ("period", "column")
lowering = xr.Dataset()
lowering["phreatic_lowering"] = xr.DataArray(np.zeros((20, 10)), coords, dims)
lowering["aquifer_lowering"] = xr.DataArray(np.zeros((20, 10)), coords, dims)
lowering["track_subsidence"] = xr.DataArray(np.full((20, 10), True), coords, dims)
lowering["phreatic_lowering"][:, 0] = 0.1
lowering["aquifer_lowering"][:, 0] = 0.1

control_unit.run_stressperiods(period_timesteps, lowering)

print(control_unit.subsidence)
