import os
import sys
import numpy as np
import pytest

import atlans
from atlans import SoilColumn, ColumnSurcharge
from atlans import ColumnInput


# If running a (PyCharm) debugger, automatically disable compilation
if "_pydev_bundle.pydev_log" in sys.modules.keys():
    os.environ["NUMBA_DISABLE_JIT"] = "1"


# Setting scope at module rather speeds up some tests: skips compilation
@pytest.fixture(scope="module")
def simple_column():
    consolidation_input = None
    oxidation_input = None
    column_input = ColumnInput(
        thickness=np.full(4, 0.25),
        zbase=-1.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    )
    return SoilColumn(column_input, consolidation_input, oxidation_input)


def test_column_checks():
    consolidation_input = None
    oxidation_input = None

    # Negative thickness
    with pytest.raises(ValueError):
        column_input = ColumnInput(
            thickness=np.full(8, -0.25),
            zbase=-2.0,
            surface_level=0.0,
            phreatic_level=-0.5,
        )
        SoilColumn(column_input, consolidation_input, oxidation_input)

    # Phreatic level below base
    with pytest.raises(ValueError):
        column_input = ColumnInput(
            thickness=np.full(8, 0.25),
            zbase=-2.0,
            surface_level=0.0,
            phreatic_level=-2.5,
        )
        SoilColumn(column_input, consolidation_input, oxidation_input)

    # Phreatic level above surface_level
    with pytest.raises(ValueError):
        column_input = ColumnInput(
            thickness=np.full(8, 0.25),
            zbase=-2.0,
            surface_level=0.0,
            phreatic_level=0.5,
        )
        SoilColumn(column_input, consolidation_input, oxidation_input)


def test_column_dummy():
    # Column contains no oxidation or consolidation whatsoever.
    # Base methods should all be present, but pass internally.
    column_input = ColumnInput(
        thickness=np.full(8, 0.25),
        zbase=-2.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    )
    consolidation_input = None
    oxidation_input = None
    soil_column = SoilColumn(column_input, consolidation_input, oxidation_input)
    # Does nothing
    soil_column.advance_timestep(1.0)
    c, o, s = soil_column.advance_stressperiod((1.0,))
    assert c == o == s == 0.0
    # Try heterogeneous timesteps
    period_timesteps = (
        np.array([1.0]),
        np.array([1.0, 2.0]),
    )
    c, o, s = atlans.run_stress_periods(soil_column, period_timesteps)
    assert np.allclose(c, np.array([0.0, 0.0, 0.0]))
    assert np.allclose(o, np.array([0.0, 0.0, 0.0]))
    assert np.allclose(s, np.array([0.0, 0.0, 0.0]))


def test_column_split_array(simple_column):
    column = simple_column
    a = np.array([1.0, 2.0])
    actual = column._split_array(a, 0, False, 0.0)
    expected = np.array([1.0, 1.0, 2.0])
    assert np.allclose(actual, expected)

    actual = column._split_array(a, 1, False, 0.0)
    expected = np.array([1.0, 2.0, 2.0])
    assert np.allclose(actual, expected)

    actual = column._split_array(a, 1, True, 0.33)
    expected = np.array([1.0, 0.33 * 2.0, 0.67 * 2.0])
    assert np.allclose(actual, expected)


def test_column_surcharge_array(simple_column):
    column = simple_column
    a = np.array([1.0, 2.0])
    to_add = a.copy()
    nlayer_old = 2
    nlayer_new = 4
    expected = np.array([1.0, 2.0, 1.0, 2.0])
    actual = column._surcharge_array(a, to_add, nlayer_old, nlayer_new)
    assert np.allclose(actual, expected)


def test_column_check_size(simple_column):
    column = simple_column
    a = np.array([1.0, 2.0])
    assert column.check_size(a, 2)
    assert not column.check_size(a, 3)


def test_column_positive_scalar(simple_column):
    column = simple_column
    assert column.check_positive_scalar(1.0)
    assert column.check_positive_scalar(0.0)
    assert not column.check_positive_scalar(-1.0)


def test_column_positive_array(simple_column):
    column = simple_column
    a = np.array([1.0, 2.0])
    assert column.check_positive_scalar(1.0)
    assert column.check_positive_scalar(0.0)
    assert not column.check_positive_scalar(-1.0)


def test_column_split_column():
    column_input = ColumnInput(
        thickness=np.full(8, 0.25),
        zbase=-2.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    )
    consolidation_input = None
    oxidation_input = None
    soil_column = SoilColumn(column_input, consolidation_input, oxidation_input)
    # Note: numba doesn't support keywords
    soil_column._split_column(0, 0.5, 9)
    assert soil_column.thickness.size == 9
    assert soil_column.z.size == 9
    assert soil_column.consolidation.size == 9
    assert soil_column.oxidation.size == 9

    expected = np.full(9, 0.25)
    expected[:2] = 0.125
    assert np.allclose(soil_column.thickness, expected)


def test_column_surcharge():
    column_input = ColumnInput(
        thickness=np.full(8, 0.25),
        zbase=-2.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    )
    consolidation_input = None
    oxidation_input = None
    thickness = np.array([0.2, 0.2])
    soil_column = SoilColumn(column_input, consolidation_input, oxidation_input)
    surcharge = ColumnSurcharge(thickness=thickness)
    soil_column._set_column_surcharge(surcharge)
    assert np.allclose(soil_column.surcharge_thickness, thickness)

    soil_column.apply_surcharge()
    assert soil_column.surface_level == 0.4
    expected = np.full(10, 0.25)
    expected[-2:] = 0.2
    assert np.allclose(soil_column.thickness, expected)
    expected = np.array([0.1, 0.3])
    assert np.allclose(soil_column.z[-2:], expected)
    assert soil_column.oxidation.size == 10
    assert soil_column.consolidation.size == 10


def test_column_lower_watertable():
    column_input = ColumnInput(
        thickness=np.full(8, 0.25),
        zbase=-2.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    )
    consolidation_input = None
    oxidation_input = None
    soil_column = SoilColumn(column_input, consolidation_input, oxidation_input)
    soil_column.phreatic_lowering = 0.1
    soil_column.aquifer_lowering = 0.1
    soil_column.lower_watertable()
    assert soil_column.phreatic_level == -0.6
