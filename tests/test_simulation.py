import os
import sys

# If running a (PyCharm) debugger, automatically disable compilation
if "_pydev_bundle.pydev_log" in sys.modules.keys():
    os.environ["NUMBA_DISABLE_JIT"] = "1"

import dask
import dask.array as da
import numpy as np
import pandas as pd
import pytest
import xarray as xr

import atlans


TIMES = np.array(
    [
        0.0,
        1.0,
        2.0,
        4.0,
        8.0,
        16.0,
        32.0,
        64.0,
        128.0,
        256.0,
        512.0,
        1024.0,
        2048.0,
        3650.0,
    ]
)
TIMESTEPS = np.diff(TIMES)


@pytest.fixture(scope="function")
def input_dataset():
    nrow = 2
    ncol = 3
    nlay = 4
    dx = 100.0
    dy = -100.0
    x = np.arange(0.0, ncol * dx, dx) + 0.5 * dx
    y = np.arange(nrow * abs(dy), 0.0, dy) + 0.5 * dy
    layer = np.arange(nlay)
    dims2d = ("y", "x")
    coords2d = {"y": y, "x": x}
    dims3d = ("y", "x", "layer")
    coords3d = {"layer": layer, "y": y, "x": x}

    ds = xr.Dataset()
    ds["thickness"] = xr.DataArray(np.full((nrow, ncol, nlay), 0.25), coords3d, dims3d)
    lithology = np.full((nrow, ncol, nlay), 1, dtype=np.int)
    lithology[:, 0, :] = 2
    lithology[0, 0, :] = -1  # missing data
    ds["lithology"] = xr.DataArray(lithology, coords3d, dims3d)
    ds["geology"] = xr.full_like(ds["lithology"], 1)
    ds["zbase"] = xr.DataArray(np.full((nrow, ncol), 0.0), coords2d, dims2d)
    ds["surface_level"] = xr.full_like(ds["zbase"], 1.0)
    ds["phreatic_level"] = xr.full_like(ds["zbase"], 0.5)
    ds["aquifer_head"] = xr.full_like(ds["zbase"], 0.5)
    ds["aquifer_top"] = xr.full_like(ds["zbase"], 0.25)  # ignore last voxel
    control_unit = np.full((nrow, ncol), 1)
    control_unit[0, 0] = -1
    ds["control_unit"] = ds["zbase"].copy(data=control_unit)

    return ds


def test_isotache_simulation(input_dataset):
    ds = input_dataset
    coords = {"period": [0], "layer": [0], "y": ds["y"], "x": ds["x"]}
    dims = ("period", "layer", "y", "x")
    surcharge = xr.Dataset()
    surcharge["thickness"] = xr.DataArray(np.full((1, 1, 2, 3), 0.4), coords, dims)
    surcharge["geology"] = xr.full_like(surcharge["thickness"], 1, dtype=np.int)
    surcharge["lithology"] = xr.full_like(surcharge["thickness"], 4, dtype=np.int)

    simulation = atlans.Simulation(
        name="simulation-test-isotache",
        dataset=ds,
        parameters="parameters.tsv",
        consolidation="isotache",
        oxidation=None,
        preconsolidation_stress="ocr",
        maximum_thickness=0.25,
    )
    simulation.open_data()
    period_timesteps = np.full((3, TIMESTEPS.size), TIMESTEPS)
    simulation.calculate(period_timesteps, lowering=None, surcharge=surcharge)
