import os
import sys
import pytest
import numpy as np

import atlans
from atlans import SoilColumn
from atlans import ColumnInput
from atlans import ColumnSurcharge
from atlans import KoppejanInput
from atlans import KoppejanSurcharge
from atlans import WaterTableLowering


# If running a (PyCharm) debugger, automatically disable compilation
if "_pydev_bundle.pydev_log" in sys.modules.keys():
    os.environ["NUMBA_DISABLE_JIT"] = "1"


@pytest.fixture(scope="function")
def koppejan_column():
    column_input = ColumnInput(
        thickness=np.full(36, 0.25),
        zbase=-12.0,
        surface_level=-3.0,
        phreatic_level=-3.0,
    )
    consolidation_input = KoppejanInput(
        gamma_wet=np.full(36, 10_500.0),
        gamma_dry=np.full(36, 10_500.0),
        preCp=np.full(36, 20.0),
        preCs=np.full(36, 80.0),
        postCp=np.full(36, 5.0),
        postCs=np.full(36, 20.0),
        c_v=np.full(36, 0.006912),
        drainage_factor=np.full(36, 1.0),
        pop_ocr=np.full(36, 10_000.0),
        aquifer_head=-3.0,
        aquifer_top=-12.0,
        pop_flag=True,
    )
    oxidation_input = None

    times = np.array(
        [
            0.0,
            1.0,
            2.0,
            4.0,
            8.0,
            16.0,
            32.0,
            64.0,
            128.0,
            256.0,
            512.0,
            1024.0,
            2048.0,
            3650.0,
        ]
    )
    timesteps = np.diff(times)
    nperiods = 20
    period_timesteps = np.full((nperiods, timesteps.size), timesteps)

    return (
        SoilColumn(column_input, consolidation_input, oxidation_input),
        period_timesteps,
    )


def test_koppejan_lowering_stressperiod(koppejan_column):
    column, period_timesteps = koppejan_column

    nperiods = period_timesteps.shape[0]
    lowering = {
        0: WaterTableLowering(
            phreatic_lowering=0.0, aquifer_lowering=0.0, track_subsidence=False
        )
    }

    # Run a single time without lowering: nothing happens in Koppejan
    consolidation_t, oxidation_t, subsidence_t = atlans.run_stress_periods(
        column, period_timesteps, lowering
    )
    assert (subsidence_t == 0.0).all()

    # Now run with an initial lowering of 0.1
    lowering = {
        0: WaterTableLowering(
            phreatic_lowering=0.1, aquifer_lowering=0.1, track_subsidence=True
        )
    }
    consolidation_t, oxidation_t, subsidence_t = atlans.run_stress_periods(
        column, period_timesteps, lowering
    )
    # Subsidence should monotonically increase
    assert (np.diff(consolidation_t) >= 0.0).all()
    assert (oxidation_t == 0.0).all()
    assert (np.diff(subsidence_t) >= 0.0).all()
    assert (consolidation_t == subsidence_t).all()


def test_koppejan_surcharge_stressperiod(koppejan_column):
    column, timesteps = koppejan_column
    column_surcharge = ColumnSurcharge(
        thickness=np.array([0.25, 0.25]),
    )
    consolidation_surcharge = KoppejanSurcharge(
        gamma_wet=np.array([17_000.0, 17_000.0]),
        gamma_dry=np.array([17_000.0, 17_000.0]),
        preCp=np.array([2000.0, 2000.0]),
        preCs=np.array([0.0, 0.0]),
        postCp=np.array([500.0, 500.0]),
        postCs=np.array([0.0, 0.0]),
        c_v=np.array([86400.0, 86400.0]),
        drainage_factor=np.array([1.0, 1.0]),
        pop_ocr=np.array([10_000.0, 10_000.0]),
    )
    oxidation_surcharge = None

    surcharge = {0: (column_surcharge, consolidation_surcharge, oxidation_surcharge)}
    (consolidation_t, oxidation_t, subsidence_t) = atlans.run_stress_periods(
        column,
        timesteps,
        None,
        surcharge,
    )
    # Subsidence should monotonically increase
    # Only a value in the first stress period
    assert consolidation_t[1] > 0.0
    assert (np.diff(consolidation_t[1:]) == 0.0).all()
    assert (oxidation_t == 0.0).all()
    assert (consolidation_t == subsidence_t).all()
