import os
import sys
import numpy as np
import pytest

import atlans


# If running a (PyCharm) debugger, automatically disable compilation

if "_pydev_bundle.pydev_log" in sys.modules.keys():
    os.environ["NUMBA_DISABLE_JIT"] = "1"


INPUTS = dict(
    column=atlans.ColumnInput(
        thickness=np.full(8, 0.25),
        zbase=-2.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    ),
    koppejan=atlans.KoppejanInput(
        gamma_wet=np.full(8, 10_500.0),
        gamma_dry=np.full(8, 10_500.0),
        preCp=np.full(8, 20.0),
        preCs=np.full(8, 80.0),
        postCp=np.full(8, 5.0),
        postCs=np.full(8, 20.0),
        c_v=np.full(8, 0.006912),
        drainage_factor=np.full(8, 1.0),
        pop_ocr=np.full(8, 10_000.0),
        aquifer_head=-0.5,
        aquifer_top=-2.0,
        pop_flag=True,
    ),
    constantrate=atlans.ConstantRateInput(
        oxidation_rate=np.full(8, 1.0e-3),
        maximum_oxidation_depth=1.2,
    ),
    carbonstore=atlans.CarbonStoreInput(
        capillary_rise=0.3,
        mass_fraction_organic=np.full(8, 0.2),
        maximum_oxidation_depth=1.2,
        minimal_mass_fraction_organic=0.05,
        oxidation_rate_alpha=np.full(8, 1.0e-3),
        rho_bulk=np.full(8, 1000.0),
    ),
    isotache=atlans.IsotacheInput(
        gamma_wet=np.full(8, 10_500.0),
        gamma_dry=np.full(8, 10_500.0),
        a=np.full(8, 0.01737),
        b=np.full(8, 0.1303),
        c=np.full(8, 0.008686),
        c_v=np.full(8, 0.006912),
        drainage_factor=np.full(8, 1.0),
        pop_ocr=np.full(8, 2.15),
        aquifer_head=-0.5,
        aquifer_top=-10.0,
        pop_flag=False,
    ),
)
INPUTS[None] = None
TIMESTEPS = np.array([1.0, 2.0, 4.0, 8.0, 16.0])
PERIOD_TIMESTEPS = np.full((3, TIMESTEPS.size), TIMESTEPS)
LOWERING = {
    0: atlans.WaterTableLowering(
        phreatic_lowering=0.0, aquifer_lowering=0.0, track_subsidence=False
    )
}
SURCHARGES = dict(
    column=atlans.ColumnSurcharge(thickness=np.array([0.25, 0.25])),
    isotache=atlans.IsotacheSurcharge(
        gamma_wet=np.array([17_000.0, 17_000.0]),
        gamma_dry=np.array([17_000.0, 17_000.0]),
        a=np.array([0.01737, 0.01737]),
        b=np.array([0.1303, 0.1303]),
        c=np.array([0.008686, 0.008686]),
        c_v=np.array([86400.0, 86400.0]),
        drainage_factor=np.array([1.0, 1.0]),
        pop_ocr=np.array([10_000.0, 10_000.0]),
    ),
    constantrate=atlans.ConstantRateSurcharge(oxidation_rate=np.full(2, 1.0e-3)),
    carbonstore=atlans.CarbonStoreSurcharge(
        mass_fraction_organic=np.full(2, 0.2),
        oxidation_rate_alpha=np.full(2, 1.0e-3),
        rho_bulk=np.full(2, 1000.0),
    ),
    koppejan=atlans.KoppejanSurcharge(
        gamma_wet=np.array([17_000.0, 17_000.0]),
        gamma_dry=np.array([17_000.0, 17_000.0]),
        preCp=np.array([2000.0, 2000.0]),
        preCs=np.array([0.0, 0.0]),
        postCp=np.array([500.0, 500.0]),
        postCs=np.array([0.0, 0.0]),
        c_v=np.array([86400.0, 86400.0]),
        drainage_factor=np.array([1.0, 1.0]),
        pop_ocr=np.array([10_000.0, 10_000.0]),
    ),
)
SURCHARGES[None] = None


@pytest.mark.parametrize("consolidation", [None, "koppejan", "isotache"])
@pytest.mark.parametrize("oxidation", [None, "constantrate", "carbonstore"])
def test_heterogeneous_columns_lowering(consolidation, oxidation):
    column_input = INPUTS["column"]
    consolidation_input = INPUTS[consolidation]
    oxidation_input = INPUTS[oxidation]
    column = atlans.SoilColumn(column_input, consolidation_input, oxidation_input)
    c, o, s = atlans.run_stress_periods(column, PERIOD_TIMESTEPS, LOWERING)


@pytest.mark.parametrize("consolidation", [None, "koppejan", "isotache"])
@pytest.mark.parametrize("oxidation", [None, "constantrate", "carbonstore"])
def test_heterogeneous_columns_surcharge(consolidation, oxidation):
    column_input = INPUTS["column"]
    consolidation_input = INPUTS[consolidation]
    oxidation_input = INPUTS[oxidation]
    column_surcharge = SURCHARGES["column"]
    consolidation_surcharge = SURCHARGES[consolidation]
    oxidation_surcharge = SURCHARGES[oxidation]
    column = atlans.SoilColumn(column_input, consolidation_input, oxidation_input)
    surcharge = {0: (column_surcharge, consolidation_surcharge, oxidation_surcharge)}
    c, o, s = atlans.run_stress_periods(
        column,
        PERIOD_TIMESTEPS,
        None,
        surcharge,
    )


@pytest.mark.parametrize("consolidation", [None, "koppejan", "isotache"])
@pytest.mark.parametrize("oxidation", [None, "constantrate", "carbonstore"])
def test_heterogeneous_columns_surcharge_lowering(consolidation, oxidation):
    column_input = INPUTS["column"]
    consolidation_input = INPUTS[consolidation]
    oxidation_input = INPUTS[oxidation]
    column_surcharge = SURCHARGES["column"]
    consolidation_surcharge = SURCHARGES[consolidation]
    oxidation_surcharge = SURCHARGES[oxidation]
    column = atlans.SoilColumn(column_input, consolidation_input, oxidation_input)
    surcharge = {0: (column_surcharge, consolidation_surcharge, oxidation_surcharge)}
    c, o, s = atlans.run_stress_periods(
        column,
        PERIOD_TIMESTEPS,
        LOWERING,
        surcharge,
    )
