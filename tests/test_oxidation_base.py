import os
import sys
import numpy as np
import pytest

from atlans import SoilColumn
from atlans import ColumnInput
from atlans import ConstantRateInput


# If running a (PyCharm) debugger, automatically disable compilation
if "_pydev_bundle.pydev_log" in sys.modules.keys():
    os.environ["NUMBA_DISABLE_JIT"] = "1"


@pytest.fixture(scope="function")
def simple_column():
    column_input = ColumnInput(
        thickness=np.full(8, 0.25),
        zbase=-2.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    )
    consolidation_input = None
    oxidation_input = ConstantRateInput(
        oxidation_rate=np.full(8, 1.0e-3),
        maximum_oxidation_depth=1.2,
    )
    return SoilColumn(column_input, consolidation_input, oxidation_input)


def test_oxidation_split_phreatic_cell(simple_column):
    column = simple_column
    column.f_oxidation_depth()
    column.split_phreatic_cell()
    # oxidation_depth is equal to cell boundary: no need to split
    assert column.thickness.size == 8
    # oxidation_depth falls in the middle of a cell, split
    column.phreatic_lowering = 0.125
    column.aquifer_lowering = 0.125
    column.f_oxidation_depth()
    column.split_phreatic_cell()
    assert column.thickness.size == 9
    # oxidation_depth doesn't exceed limit
    column.phreatic_lowering = 0.005
    column.aquifer_lowering = 0.005
    column.f_oxidation_depth()
    column.split_phreatic_cell()
    assert column.thickness.size == 9
