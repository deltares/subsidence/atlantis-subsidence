import os
import sys
import numpy as np
import pytest

from atlans import SoilColumn
from atlans import ColumnInput
from atlans import ColumnSurcharge
from atlans import ConstantRateInput
from atlans import ConstantRateSurcharge


# If running a (PyCharm) debugger, automatically disable compilation
if "_pydev_bundle.pydev_log" in sys.modules.keys():
    os.environ["NUMBA_DISABLE_JIT"] = "1"


@pytest.fixture(scope="function")
def simple_column():
    column_input = ColumnInput(
        thickness=np.full(8, 0.25),
        zbase=-2.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    )
    consolidation_input = None
    oxidation_input = ConstantRateInput(
        oxidation_rate=np.full(8, 1.0e-3),
        maximum_oxidation_depth=1.2,
    )
    return SoilColumn(column_input, consolidation_input, oxidation_input)


def test_constantrate_checks():
    column_input = ColumnInput(
        thickness=np.full(8, 0.25),
        zbase=-2.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    )
    consolidation_input = None

    # Misshapen oxidation_rate
    with pytest.raises(ValueError):
        oxidation_input = ConstantRateInput(
            oxidation_rate=np.full(10, 1.0e-3),
            maximum_oxidation_depth=1.2,
        )
        SoilColumn(column_input, consolidation_input, oxidation_input)

    # Negative oxidation rate
    with pytest.raises(ValueError):
        oxidation_input = ConstantRateInput(
            oxidation_rate=np.full(8, -1.0e-3),
            maximum_oxidation_depth=1.2,
        )
        SoilColumn(column_input, consolidation_input, oxidation_input)

    # Negative maximum_depth
    with pytest.raises(ValueError):
        oxidation_input = ConstantRateInput(
            oxidation_rate=np.full(8, 1.0e-3),
            maximum_oxidation_depth=-1.2,
        )
        SoilColumn(column_input, consolidation_input, oxidation_input)


def test_constantrate_surcharge(simple_column):
    column = simple_column
    column_surcharge = ColumnSurcharge(thickness=np.full(2, 0.20))
    consolidation_surcharge = ()
    oxidation_surcharge = ConstantRateSurcharge(oxidation_rate=np.full(2, 1.0e-3))
    column.set_surcharge(column_surcharge, consolidation_surcharge, oxidation_surcharge)
    column.apply_surcharge()
    assert column.oxidation_rate.size == 10


def test_constantrate_oxidation_depth(simple_column):
    column = simple_column
    column.phreatic_lowering = 0.25
    column.aquifer_lowering = 0.25
    column.f_oxidation_depth()
    assert column.oxidation_depth == -0.75
    column.phreatic_lowering = 2.0
    column.aquifer_lowering = 2.0
    column.f_oxidation_depth()
    assert column.oxidation_depth == -1.2


def test_constantrate_oxidation():
    column_input = ColumnInput(
        thickness=np.full(8, 0.25),
        zbase=-2.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    )
    consolidation_input = None
    oxidation_input = ConstantRateInput(
        oxidation_rate=np.full(8, 1.0e-3),
        maximum_oxidation_depth=1.2,
    )
    soil_column = SoilColumn(column_input, consolidation_input, oxidation_input)
    soil_column.phreatic_lowering = 0.0
    soil_column.aquifer_lowering = 0.0
    timestep = 1.0
    soil_column.advance_stressperiod(np.array([timestep]))
    rate = 1.0e-3
    dz = 0.25
    change = dz * (1.0 - np.exp(-rate * timestep))
    assert np.allclose(
        soil_column.oxidation, np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, change, change])
    )

    # Set oxidation rate extremely high
    # Change should not exceed cell thickness
    oxidation_input = ConstantRateInput(
        oxidation_rate=np.full(8, 1000.0),
        maximum_oxidation_depth=1.2,
    )
    soil_column = SoilColumn(column_input, consolidation_input, oxidation_input)
    soil_column.phreatic_lowering = 0.0
    soil_column.aquifer_lowering = 0.0
    soil_column.advance_stressperiod(np.array([timestep]))
    assert np.allclose(
        soil_column.oxidation, np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, dz, dz])
    )
