import os
import sys
import numpy as np
import pytest

from atlans import SoilColumn
from atlans import ColumnInput
from atlans import ColumnSurcharge
from atlans import IsotacheInput
from atlans import IsotacheSurcharge


# If running a (PyCharm) debugger, automatically disable compilation
if "_pydev_bundle.pydev_log" in sys.modules.keys():
    os.environ["NUMBA_DISABLE_JIT"] = "1"


# We're using IsotacheConsolidation here, since the BaseType is abstract
# and does not work on it's own.
# No Isotache methods are tested here, however.
# Setting scope at module rather speeds up some tests: skips compilation
@pytest.fixture(scope="module")
def isotache_column():
    def create_isotache_column(size):
        column_input = ColumnInput(
            thickness=np.full(size, 0.25),
            zbase=-1.0,
            surface_level=0.0,
            phreatic_level=-0.5,
        )
        consolidation_input = IsotacheInput(
            a=np.full(size, 1.0e-6),
            b=np.full(size, 1.0e-6),
            c=np.full(size, 1.0e-6),
            gamma_wet=np.full(size, 19_000.0),
            gamma_dry=np.full(size, 16_000.0),
            c_v=np.full(size, 1.0e-5),
            drainage_factor=np.full(size, 2.0),
            pop_ocr=np.full(size, 1.5),
            pop_flag=False,
            aquifer_head=-0.5,
            aquifer_top=-1.0,
        )
        oxidation_input = None
        return SoilColumn(column_input, consolidation_input, oxidation_input)

    return create_isotache_column


def test_consolidation_initialize(isotache_column):
    column = isotache_column(4)
    # Column states
    assert column.thickness.size == 4
    assert column.z.size == 4
    assert column.oxidation.size == 4
    assert column.consolidation.size == 4
    # Consolidation states
    assert column.gamma_water == 9810.0
    assert column.pore_pressure.size == 4
    assert column.total_stress.size == 4
    assert column.effective_stress.size == 4
    assert column.previous_effective_stress.size == 4
    assert column.preconsolidation_stress.size == 4
    assert column.delta_U.size == 4
    assert column.strain.size == 4
    assert column.gamma_wet.size == 4
    assert column.gamma_dry.size == 4
    assert column.c_v.size == 4
    assert column.drainage_factor.size == 4
    assert column.pop_ocr.size == 4
    assert column.aquifer_head == -0.5
    assert column.aquifer_top == -1.0
    assert column.pop_flag == 0


def test_consolidation_surcharge(isotache_column):
    column = isotache_column(4)
    column_surcharge = ColumnSurcharge(thickness=np.full(2, 0.20))
    isotache_surcharge = IsotacheSurcharge(
        a=np.full(2, 1.0e-6),
        b=np.full(2, 1.0e-6),
        c=np.full(2, 1.0e-6),
        gamma_wet=np.full(2, 18_000.0),
        gamma_dry=np.full(2, 18_000.0),
        c_v=np.full(2, 1.0e-5),
        drainage_factor=np.full(2, 2.0),
        pop_ocr=np.full(2, 1.5),
    )
    column.set_surcharge(column_surcharge, isotache_surcharge, None)
    column.apply_surcharge()
    # Do same checks, except arrays should be size 6 now
    assert column.thickness.size == 6
    assert column.z.size == 6
    assert column.oxidation.size == 6
    assert column.consolidation.size == 6
    # Consolidation states
    assert column.gamma_water == 9810.0
    assert column.pore_pressure.size == 6
    assert column.total_stress.size == 6
    assert column.effective_stress.size == 6
    assert column.previous_effective_stress.size == 6
    assert column.preconsolidation_stress.size == 6
    assert column.delta_U.size == 6
    assert column.strain.size == 6
    assert column.gamma_wet.size == 6
    assert column.gamma_dry.size == 6
    assert column.c_v.size == 6
    assert column.drainage_factor.size == 6
    assert column.pop_ocr.size == 6
    assert column.aquifer_head == -0.5
    assert column.aquifer_top == -1.0
    assert column.pop_flag == 0


def test_consolidation_stress(isotache_column):
    column = isotache_column(5)  # Set test variables
    column.z = np.arange(0.0, 3.0, 0.5)
    column.aquifer_head = 3.0
    column.aquifer_top = 1.0
    column.phreatic_level = 2.0
    column.surface_level = 2.5

    # Pore pressure
    column.f_pore_pressure()
    pressure_head = column.pore_pressure / (9.81 * 1000.0)
    assert np.allclose(pressure_head, np.array([3.0, 2.5, 2.0, 1.0, 0.0, 0.0]))

    # Total stress
    column.gamma_wet = np.full(5, 1000.0)
    column.gamma_dry = np.full(5, 500.0)
    column.thickness = np.full(5, 1.0)
    column.f_total_stress()
    assert np.allclose(
        column.total_stress, np.array([4250.0, 3250.0, 2250.0, 1250.0, 250.0])
    )

    # Effective stress
    column.total_stress = np.arange(5000.0, 0.0, -1000.0)
    column.pore_pressure = np.array([4000.0, 3000.0, 2000.0, 1000.0, 0.0])
    column.f_effective_stress()
    assert np.allclose(
        column.effective_stress, column.total_stress - column.pore_pressure
    )

    # Preconsolidation stress, POP
    column.pop_ocr = np.full(5, 10_000.0)
    column.pop_flag = True
    column.effective_stress = np.full(5, 10_000.0)
    column.f_preconsolidation_stress()
    assert np.allclose(column.preconsolidation_stress, np.full(5, 20_000.0))

    # Preconsolidation stress, OCR
    column.pop_ocr = np.full(5, 2.0)
    column.pop_flag = False
    column.effective_stress = np.full(5, 1000.0)
    column.f_preconsolidation_stress()
    assert np.allclose(column.preconsolidation_stress, np.full(5, 2000.0))


def test_consolidation__degree_of_consolidation_U(isotache_column):
    column = isotache_column(5)
    column.thickness = np.full(5, 0.5)
    column.c_v = np.full(5, 0.002592)
    column.drainage_factor = np.full(5, 2.0)
    time = 16.0
    U = column.f_U(time)
    assert np.allclose(U, np.full(5, 0.22858032), atol=1.0e-9)

    column.previous_time = 8.0
    column.f_delta_U()
    assert np.allclose(
        column.delta_U,
        column.f_U(column.time) - column.f_U(column.previous_time),
    )


def test_consolidation_update_gamma(isotache_column):
    column = isotache_column(5)
    column.thickness = np.full(5, 0.5)
    column.gamma_wet = np.full(5, 15_000.0)
    column.consolidation = np.full(5, 0.1)
    column.f_update_gamma()
    assert np.array_equal(column.gamma_wet, np.full(5, 16297.5))
