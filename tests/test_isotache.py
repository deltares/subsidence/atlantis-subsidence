import os
import sys
import numpy as np
import pytest

import atlans
from atlans import SoilColumn
from atlans import ColumnInput
from atlans import ColumnSurcharge
from atlans import IsotacheInput
from atlans import IsotacheSurcharge
from atlans import WaterTableLowering


# If running a (PyCharm) debugger, automatically disable compilation
if "_pydev_bundle.pydev_log" in sys.modules.keys():
    os.environ["NUMBA_DISABLE_JIT"] = "1"


@pytest.fixture(scope="function")
def isotache_column():
    column_input = ColumnInput(
        thickness=np.full(20, 0.5),
        zbase=-10.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    )
    consolidation_input = IsotacheInput(
        gamma_wet=np.full(20, 10_500.0),
        gamma_dry=np.full(20, 10_500.0),
        a=np.full(20, 0.01737),
        b=np.full(20, 0.1303),
        c=np.full(20, 0.008686),
        c_v=np.full(20, 0.006912),
        drainage_factor=np.full(20, 1.0),
        pop_ocr=np.full(20, 2.15),
        aquifer_head=-0.5,
        aquifer_top=-10.0,
        pop_flag=False,
    )
    oxidation_input = None

    times = np.array(
        [
            0.0,
            1.0,
            2.0,
            4.0,
            8.0,
            16.0,
            32.0,
            64.0,
            128.0,
            256.0,
            512.0,
            1024.0,
            2048.0,
            3650.0,
        ]
    )
    timesteps = np.diff(times)
    nperiods = 20
    period_timesteps = np.full((nperiods, timesteps.size), timesteps)
    return (
        SoilColumn(column_input, consolidation_input, oxidation_input),
        period_timesteps,
    )


def test_isotache_lowering_stressperiods(isotache_column):
    column, period_timesteps = isotache_column
    nperiods = period_timesteps.shape[0]

    lowering = {
        0: WaterTableLowering(
            phreatic_lowering=0.0, aquifer_lowering=0.0, track_subsidence=False
        )
    }
    # Run a single time without lowering: something happens in isotache
    consolidation_t, oxidation_t, subsidence_t = atlans.run_stress_periods(
        column, period_timesteps, lowering
    )
    # Subsidence should monotonically increase
    assert (np.diff(consolidation_t) > 0.0).all()
    assert (oxidation_t == 0.0).all()
    assert (np.diff(subsidence_t) > 0.0).all()
    assert (consolidation_t == subsidence_t).all()

    # Now present a lowering
    lowering = {0: WaterTableLowering(0.5, 0.5, False)}

    # Run method
    consolidation_t, oxidation_t, subsidence_t = atlans.run_stress_periods(
        column, period_timesteps, lowering
    )
    # Subsidence should monotonically increase
    assert (np.diff(consolidation_t) > 0.0).all()
    assert (oxidation_t == 0.0).all()
    assert (np.diff(subsidence_t) > 0.0).all()
    assert (consolidation_t == subsidence_t).all()


def test_isotache_surcharge_stressperiods(isotache_column):
    column, timesteps = isotache_column
    column_surcharge = ColumnSurcharge(
        thickness=np.array([0.25, 0.25]),
    )
    consolidation_surcharge = IsotacheSurcharge(
        gamma_wet=np.array([17_000.0, 17_000.0]),
        gamma_dry=np.array([17_000.0, 17_000.0]),
        a=np.array([0.01737, 0.01737]),
        b=np.array([0.1303, 0.1303]),
        c=np.array([0.008686, 0.008686]),
        c_v=np.array([86400.0, 86400.0]),
        drainage_factor=np.array([1.0, 1.0]),
        pop_ocr=np.array([10_000.0, 10_000.0]),
    )
    oxidation_surcharge = None

    surcharge = {0: (column_surcharge, consolidation_surcharge, oxidation_surcharge)}
    consolidation_t, oxidation_t, subsidence_t = atlans.run_stress_periods(
        column, timesteps, None, surcharge
    )
    # Subsidence should monotonically increase
    # Only a value in the first stress period
    assert (np.diff(consolidation_t) > 0.0).all()
    assert (oxidation_t == 0.0).all()
    assert (consolidation_t == subsidence_t).all()
