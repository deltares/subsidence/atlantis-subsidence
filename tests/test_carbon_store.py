import os
import sys
import numpy as np
import pytest

from atlans import SoilColumn
from atlans import ColumnInput
from atlans import ColumnSurcharge
from atlans import CarbonStoreInput
from atlans import CarbonStoreSurcharge


# If running a (PyCharm) debugger, automatically disable compilation
if "_pydev_bundle.pydev_log" in sys.modules.keys():
    os.environ["NUMBA_DISABLE_JIT"] = "1"


@pytest.fixture(scope="function")
def simple_column():
    column_input = ColumnInput(
        thickness=np.full(8, 0.25),
        zbase=-2.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    )
    consolidation_input = None
    oxidation_input = CarbonStoreInput(
        capillary_rise=0.3,
        mass_fraction_organic=np.full(8, 0.2),
        maximum_oxidation_depth=1.2,
        minimal_mass_fraction_organic=0.05,
        oxidation_rate_alpha=np.full(8, 1.0e-3),
        rho_bulk=np.full(8, 1000.0),
    )
    return SoilColumn(column_input, consolidation_input, oxidation_input)


@pytest.mark.parametrize(
    "parameter",
    [
        "capillary_rise",
        "mass_fraction_organic",
        "maximum_oxidation_depth",
        "minimal_mass_fraction_organic",
        "oxidation_rate_alpha",
        "rho_bulk",
    ],
)
def test_carbonstore_checks(parameter):
    column_input = ColumnInput(
        thickness=np.full(8, 0.25),
        zbase=-2.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    )
    input_d = dict(
        capillary_rise=0.3,
        mass_fraction_organic=np.full(8, 0.2),
        maximum_oxidation_depth=1.2,
        minimal_mass_fraction_organic=0.05,
        oxidation_rate_alpha=np.full(8, 1.0e-3),
        rho_bulk=np.full(8, 1000.0),
    )
    consolidation_input = None

    # Misshapen
    d = input_d.copy()
    if isinstance(d[parameter], np.ndarray):
        d[parameter] = np.resize(d[parameter], 10)
        oxidation_input = CarbonStoreInput(**d)
        with pytest.raises(ValueError):
            SoilColumn(column_input, consolidation_input, oxidation_input)

    # Negative values
    d = input_d.copy()
    d[parameter] = -1.0 * d[parameter]
    oxidation_input = CarbonStoreInput(**d)
    with pytest.raises(ValueError):
        SoilColumn(column_input, consolidation_input, oxidation_input)


def test_carbonstore_split_phreatic(simple_column):
    column = simple_column
    # No change
    column.phreatic_lowering = 0.3  # compensate for capillary rise
    column.aquifer_lowering = 0.3  # compensate for capillary rise
    column.f_oxidation_depth()
    column.split_phreatic_cell()
    # Check all arrays
    assert column.mass_fraction_organic.size == 8
    assert column.oxidation_rate_alpha.size == 8
    assert column.rho_bulk.size == 8
    assert column.mass_organic.size == 8
    assert column.mass_mineral.size == 8
    assert column.minimal_mass_organic.size == 8
    assert column.oxidation_rate_alpha.size == 8
    assert column.rho_bulk.size == 8
    assert column.specific_volume.size == 8

    # Middle of cell
    column.phreatic_lowering = 0.125
    column.aquifer_lowering = 0.125
    column.f_oxidation_depth()
    column.split_phreatic_cell()
    # Check all arrays
    assert column.mass_fraction_organic.size == 9
    assert column.oxidation_rate_alpha.size == 9
    assert column.rho_bulk.size == 9
    assert column.mass_organic.size == 9
    assert column.mass_mineral.size == 9
    assert column.minimal_mass_organic.size == 9
    assert column.oxidation_rate_alpha.size == 9
    assert column.rho_bulk.size == 9
    assert column.specific_volume.size == 9


def test_carbonstore_surcharge(simple_column):
    column = simple_column
    column_surcharge = ColumnSurcharge(thickness=np.full(2, 0.20))
    consolidation_surcharge = ()
    oxidation_surcharge = CarbonStoreSurcharge(
        mass_fraction_organic=np.full(2, 0.2),
        oxidation_rate_alpha=np.full(2, 1.0e-3),
        rho_bulk=np.full(2, 1000.0),
    )
    column.set_surcharge(column_surcharge, consolidation_surcharge, oxidation_surcharge)
    column.apply_surcharge()
    # Check all arrays
    assert column.mass_fraction_organic.size == 10
    assert column.oxidation_rate_alpha.size == 10
    assert column.rho_bulk.size == 10
    assert column.mass_organic.size == 10
    assert column.mass_mineral.size == 10
    assert column.minimal_mass_organic.size == 10
    assert column.oxidation_rate_alpha.size == 10
    assert column.rho_bulk.size == 10
    assert column.specific_volume.size == 10


def test_carbonstore_oxidation_depth(simple_column):
    column = simple_column
    column.f_oxidation_depth()
    # phreatic level of -0.5, capillary rise of 0.3
    assert column.oxidation_depth == -0.2
    column.phreatic_lowering = 2.0
    column.aquifer_lowering = 2.0
    column.f_oxidation_depth()
    assert column.oxidation_depth == -1.2


def test_carbonstore_oxidation(simple_column):
    column = simple_column
    timestep = 1.0
    column.advance_stressperiod(np.array([timestep]))
    assert (column.oxidation >= 0).all()


def test_carbonstore_oxidation__low_initial_organic(simple_column):
    timestep = 1.0
    column_input = ColumnInput(
        thickness=np.full(8, 0.25),
        zbase=-2.0,
        surface_level=0.0,
        phreatic_level=-0.5,
    )
    consolidation_input = None
    oxidation_input = CarbonStoreInput(
        capillary_rise=0.3,
        mass_fraction_organic=np.full(8, 0.01),
        maximum_oxidation_depth=1.2,
        minimal_mass_fraction_organic=0.05,
        oxidation_rate_alpha=np.full(8, 1.0e-3),
        rho_bulk=np.full(8, 1000.0),
    )
    column = SoilColumn(column_input, consolidation_input, oxidation_input)
    column.advance_stressperiod(np.array([timestep]))
    assert (column.oxidation >= 0).all()
