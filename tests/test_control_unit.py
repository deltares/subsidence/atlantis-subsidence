import os
import sys

# If running a (PyCharm) debugger, automatically disable compilation
if "_pydev_bundle.pydev_log" in sys.modules.keys():
    os.environ["NUMBA_DISABLE_JIT"] = "1"

import dask
import dask.array as da
import numpy as np
import pandas as pd
import pytest
import xarray as xr

import atlans


TIMES = np.array(
    [
        0.0,
        1.0,
        2.0,
        4.0,
        8.0,
        16.0,
        32.0,
        64.0,
        128.0,
        256.0,
        512.0,
        1024.0,
        2048.0,
        3650.0,
    ]
)
TIMESTEPS = np.diff(TIMES)


@pytest.fixture(scope="function")
def control_unit_koppejan(request):
    """
    Create a control unit of 10 identical columns, like in test_koppejan.
    """
    ncol = 10
    nlayer = 18

    coords2d = {"column": range(ncol), "layer": range(nlayer)}
    dims2d = ("column", "layer")
    coords1d = {"column": range(ncol)}
    dims1d = ("column",)

    dataset = xr.Dataset()
    dataset["thickness"] = xr.DataArray(
        data=np.full((ncol, nlayer), 0.5), coords=coords2d, dims=dims2d
    )
    dataset["lithology"] = xr.DataArray(
        data=np.full((ncol, nlayer), 1, dtype=np.int8), coords=coords2d, dims=dims2d
    )
    dataset["geology"] = xr.DataArray(
        data=np.full((ncol, nlayer), 1, dtype=np.int8), coords=coords2d, dims=dims2d
    )
    dataset["aquifer_head"] = xr.DataArray(
        data=np.full(ncol, -3.0), coords=coords1d, dims=dims1d
    )
    dataset["aquifer_top"] = xr.DataArray(
        data=np.full(ncol, -12.0), coords=coords1d, dims=dims1d
    )
    dataset["surface_level"] = xr.DataArray(
        data=np.full(ncol, -3.0), coords=coords1d, dims=dims1d
    )
    dataset["phreatic_level"] = xr.DataArray(
        data=np.full(ncol, -3.0), coords=coords1d, dims=dims1d
    )
    dataset["zbase"] = xr.DataArray(
        data=np.full(ncol, -12.0), coords=coords1d, dims=dims1d
    )
    dataset["water_surface"] = xr.DataArray(
        data=np.full(ncol, False), coords=coords1d, dims=dims1d
    )
    dataset["maximum_oxidation_depth"] = xr.DataArray(
        data=np.full(ncol, 1.2), coords=coords1d, dims=dims1d
    )

    parameters = {
        "lithology": np.array([1], dtype=np.int8),
        "geology": np.array([1], dtype=np.int8),
        "gamma_wet": np.array([10_500.0]),
        "gamma_dry": np.array([3_500.0]),
        "preCp": np.array([20.0]),
        "preCs": np.array([80.0]),
        "postCs": np.array([5.0]),
        "postCp": np.array([20.0]),
        "c_v": np.array([0.006912]),
        "drainage_factor": np.array([1.0]),
        "pop_ocr": np.array([10_000.0]),
        "oxidation_rate": np.array([4.1095890410958905e-05]),
    }

    options = {
        "pop_flag": True,
        "maximum_thickness": 0.25,
        "consolidation": "koppejan",
        "oxidation": "constant_rate",
    }

    control_unit = atlans.ControlUnit(
        dataset=dataset, parameters=parameters, options=options
    )

    return control_unit


@pytest.fixture(scope="function")
def surcharge(request):
    """
    Create a control unit of 10 identical columns, like in test_koppejan.
    """
    ncol = 10
    nlayer = 3

    coords2d = {"period": [0], "column": range(ncol), "layer": range(nlayer)}
    dims2d = ("period", "column", "layer")

    surcharge_ds = xr.Dataset()
    surcharge_ds["thickness"] = xr.DataArray(
        data=np.full((1, ncol, nlayer), 0.25), coords=coords2d, dims=dims2d
    )
    surcharge_ds["lithology"] = xr.DataArray(
        data=np.full((1, ncol, nlayer), 1, dtype=np.int8), coords=coords2d, dims=dims2d
    )
    surcharge_ds["geology"] = xr.DataArray(
        data=np.full((1, ncol, nlayer), 1, dtype=np.int8), coords=coords2d, dims=dims2d
    )
    return surcharge_ds


@pytest.fixture(scope="function")
def control_unit_isotache(request):
    """
    Create a control unit of 10 identical columns, like in test_koppejan.
    """
    ncol = 10
    nlayer = 20

    coords2d = {"column": range(ncol), "layer": range(nlayer)}
    dims2d = ("column", "layer")
    coords1d = {"column": range(ncol)}
    dims1d = ("column",)

    dataset = xr.Dataset()
    dataset["thickness"] = xr.DataArray(
        data=np.full((ncol, nlayer), 0.5), coords=coords2d, dims=dims2d
    )
    dataset["lithology"] = xr.DataArray(
        data=np.full((ncol, nlayer), 1, dtype=np.int8), coords=coords2d, dims=dims2d
    )
    dataset["geology"] = xr.DataArray(
        data=np.full((ncol, nlayer), 1, dtype=np.int8), coords=coords2d, dims=dims2d
    )
    dataset["aquifer_head"] = xr.DataArray(
        data=np.full(ncol, -0.5), coords=coords1d, dims=dims1d
    )
    dataset["aquifer_top"] = xr.DataArray(
        data=np.full(ncol, -10.0), coords=coords1d, dims=dims1d
    )
    dataset["surface_level"] = xr.DataArray(
        data=np.full(ncol, 0.0), coords=coords1d, dims=dims1d
    )
    dataset["phreatic_level"] = xr.DataArray(
        data=np.full(ncol, -0.5), coords=coords1d, dims=dims1d
    )
    dataset["zbase"] = xr.DataArray(
        data=np.full(ncol, -10.0), coords=coords1d, dims=dims1d
    )
    dataset["water_surface"] = xr.DataArray(
        data=np.full(ncol, False), coords=coords1d, dims=dims1d
    )
    dataset["maximum_oxidation_depth"] = xr.DataArray(
        data=np.full(ncol, 1.2), coords=coords1d, dims=dims1d
    )

    parameters = {
        "lithology": np.array([1], dtype=np.int8),
        "geology": np.array([1], dtype=np.int8),
        "gamma_wet": np.array([10_500.0]),
        "gamma_dry": np.array([3_500.0]),
        "a": np.array([0.01737]),
        "b": np.array([0.1303]),
        "c": np.array([0.008686]),
        "c_v": np.array([0.006912]),
        "drainage_factor": np.array([1.0]),
        "pop_ocr": np.array([2.15]),
        "oxidation_rate": np.array([0.0]),
    }

    options = {
        "pop_flag": False,
        "maximum_thickness": 0.50,
        "consolidation": "isotache",
        "oxidation": "constant_rate",
    }

    control_unit = atlans.ControlUnit(
        dataset=dataset, parameters=parameters, options=options
    )

    return control_unit


def test_dataset_to_namedtuple(control_unit_koppejan):
    control_unit = control_unit_koppejan
    control_unit.dataset_to_namedtuple()
    assert hasattr(control_unit, "column_input")
    assert hasattr(control_unit, "consolidation_input")
    assert hasattr(control_unit, "oxidation_input")


def test_dataset_make_create_column(control_unit_koppejan):
    control_unit = control_unit_koppejan
    control_unit.dataset_to_namedtuple()
    control_unit._make_create_columns()
    control_unit._columns_with_data()

    soil_columns = control_unit.create_columns(
        control_unit.column_input,
        control_unit.consolidation_input,
        control_unit.oxidation_input,
        control_unit.options["maximum_thickness"],
        control_unit._has_data,
    )

    # test whether thicknesses have been initalized properly
    for soil_column in soil_columns:
        assert np.allclose(soil_column.thickness, 0.25)

    subsidence = []
    for soil_column in soil_columns:
        _, _, s = soil_column.advance_stressperiod(TIMESTEPS)
        subsidence.append(s)

    assert all([s == subsidence[0] for s in subsidence])


def test_run_stressperiods(control_unit_koppejan, surcharge):
    control_unit = control_unit_koppejan
    # repeat twenty times
    period_timesteps = np.vstack([TIMESTEPS for _ in range(20)])
    coords = {"period": range(20), "column": range(10)}
    dims = ("period", "column")
    lowering = xr.Dataset()
    lowering["phreatic_lowering"] = xr.DataArray(np.zeros((20, 10)), coords, dims)
    lowering["aquifer_lowering"] = xr.DataArray(np.zeros((20, 10)), coords, dims)
    lowering["track_subsidence"] = xr.DataArray(np.full((20, 10), True), coords, dims)
    lowering["phreatic_lowering"][:, 0] = 0.1
    lowering["aquifer_lowering"][:, 0] = 0.1

    control_unit.run_stressperiods(period_timesteps, lowering)
    output = control_unit.output()
    assert output["subsidence"].shape == (10, 20)
    assert output["consolidation"].shape == (10, 20)
    assert output["oxidation"].shape == (10, 20)

    # Re-initialize
    lowering["phreatic_lowering"] = xr.DataArray(np.zeros((20, 10)), coords, dims)
    lowering["aquifer_lowering"] = xr.DataArray(np.zeros((20, 10)), coords, dims)
    lowering["track_subsidence"] = xr.DataArray(np.full((20, 10), False), coords, dims)
    control_unit = control_unit_koppejan
    control_unit.run_stressperiods(period_timesteps, lowering, surcharge)


def test_run_stressperiods_koppejan(control_unit_koppejan):
    control_unit = control_unit_koppejan
    # repeat twenty times
    period_timesteps = np.vstack([TIMESTEPS for _ in range(20)])
    coords = {"period": range(20), "column": range(10)}
    dims = ("period", "column")
    lowering = xr.Dataset()
    lowering["phreatic_lowering"] = xr.DataArray(np.zeros((20, 10)), coords, dims)
    lowering["aquifer_lowering"] = xr.DataArray(np.zeros((20, 10)), coords, dims)
    lowering["track_subsidence"] = xr.DataArray(np.full((20, 10), True), coords, dims)
    lowering["phreatic_lowering"][:, 0] = 0.1
    lowering["aquifer_lowering"][:, 0] = 0.1

    control_unit.run_stressperiods(period_timesteps, lowering)
    output = control_unit.output()
    assert isinstance(output, xr.Dataset)


def test_run_stressperiods_isotache(control_unit_isotache):
    control_unit = control_unit_isotache
    # repeat twenty times
    period_timesteps = np.vstack([TIMESTEPS for _ in range(20)])
    coords = {"period": range(20), "column": range(10)}
    dims = ("period", "column")
    lowering = xr.Dataset()
    lowering["phreatic_lowering"] = xr.DataArray(np.zeros((20, 10)), coords, dims)
    lowering["aquifer_lowering"] = xr.DataArray(np.zeros((20, 10)), coords, dims)
    lowering["track_subsidence"] = xr.DataArray(np.full((20, 10), False), coords, dims)
    lowering["phreatic_lowering"][:, 0] = 0.5
    lowering["aquifer_lowering"][:, 0] = 0.5

    control_unit.run_stressperiods(period_timesteps, lowering)
    output = control_unit.output()
    assert isinstance(output, xr.Dataset)

    # force it to compute
    subsidence = output["subsidence"].values
