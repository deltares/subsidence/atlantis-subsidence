from setuptools import setup, find_packages

try:
    import pypandoc

    long_description = pypandoc.convert("README.md", "rst")
except ImportError:
    long_description = ""

setup(
    name="atlantis-subsidence",
    version="0.1",
    description="Soil subsidence modeling",
    long_description=long_description,
    url="https://gitlab.com/Huite/atlantis-subsidence",
    author="Huite Bootsma",
    author_email="huite.bootsma@deltares.nl",
    license="MIT",
    packages=find_packages(),
    test_suite="tests",
    python_requires=">=3.6",
    install_requires=[
        "cytoolz",
        "dask",
        "netcdf4",
        "numba",
        "numpy",
        "pandas",
        "toolz",
        "xarray",
    ],
    extras_require={"dev": ["pytest", "sphinx", "sphinx_rtd_theme"]},
    classifiers=[
        # https://pypi.python.org/pypi?%3Aaction=list_classifiers
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
    ],
)
