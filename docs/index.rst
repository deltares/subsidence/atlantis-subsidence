Atlantis Subsidence (atlans): Soil subsidence calculations in Python
====================================================================

**atlans**, short for Atlantis Subsidence, is a Python package to calculate soil
(1D) subsidence using voxel based geological lithological models, such as GeoTOP
(Stafleu, ...).

atlans can be used for calculating soil consolidation and peat oxidation.
Currently, only Koppejan's method (Koppejan (...), De Lange (...)) has been
implemented. Methods for oxidation calculations are based on Van den Akker
(...)

atlans makes use of several (scientific) Python packages to handle
larger-than-memory datasets and to achieve reasonable performance without
resorting to languages other than Python:

* `xarray`
* `numba`
* `dask`
* `netCDF4`

Documentation
-------------

**Getting Started**

* :doc:`introduction`
* :doc:`examples`
* :doc:`installing`

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Getting Started

   introduction
   examples
   installing

**Reference**

* :doc:`internals`

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Reference

   internals

.. toctree::
   :maxdepth: 4
   :caption: Modules

   atlans.rst

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

License
-------

atlans is available under the open source `MIT license <https://opensource.org/licenses/MIT>`_.
