.. _introduction:

Introduction
============

**atlans**, short for Atlantis Subsidence, is a Python package to calculate soil
(1D) subsidence using voxel based geological lithological models, such as GeoTOP
(Stafleu, ...).

atlans can be used for calculating soil consolidation and peat oxidation.
Currently, only Koppejan's method (Koppejan (...), De Lange (...)) has been
implemented. Methods for oxidation calculations are based on Van den Akker
(...)
