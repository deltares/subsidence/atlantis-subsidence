.. _installing:

Installation
============

atlans is a pure Python package, but its dependencies are not: `numba`
requires the LLVM compiler.
Direct installation via `pip` is possible, but not recommended. `pip`
installations are generally sufficient for the simple use case of interactive
calculations of a single soil column, but not for spatially distributed
simulations. Spatially distributed computations require `dask` which is much
easier to install with `conda` instead.

Using the conda command line tool, run:

    $ conda install -c conda-forge pandas xarray dask numba

This should install all required dependencies for atlans. You may want to 
create a separate "atlantis" conda environment to install atlans. This isolates
it from existing Python installations and simplifies dependency maintenance. You
can do so by saving the attached `environment.yml` and running:

    $ conda create env -f environment.yml

This will create a conda environment named `atlantis` containing all the required
packages. To activate this environment, simply run the following command in your
command line:

    $ activate atlantis

When this environment is active, the Python executable together with the
specific version of the packages within this environment will be used to execute
Python programs.

To return to your `base` environment, run:

    $ deactivate

Or:

    $ activate base

Refer to the `conda documentation <https://conda.io/docs/user-guide/tasks/manage-environments.html>`_ for more information.