.. _internals:

atlans internals
================

Background
----------

The backbones of this package are numpy, numba, dask, and xarray:

* numpy provides data structures and methods for numerical arrays.
* numba accelerates Python code to achieve similar performance to C, C++, or
  Fortran.
* dask provides chunked data structures to deal with larger-than-memory arrays
  and simple tools for parallel computation.
* xarray provides chunkwise input/output to NetCDF4/HDF5 files. Xarray wraps
  dask arrays and numpy arrays, allowing for easy streaming of data.

Atlans is aimed at two kinds of use:

1. Interactive, exploratory calculations for a single soil column.
2. Spatially distributed calculations for one or more management areas.

To this end, atlans contains three primary data structures:

1. The individual soil column. A soil column can be flexibly defined from
  multiple conceptual models.
2. A single management area. A management area consists of a collection of soil
  columns. Optionally, any lowering of the water table is is executed across
  all the columns of the management area.
3. The simulation. The simulation contains the data for a full spatial
  computation, and generally consists of multiple management areas.

Interactive use
---------------

Atlans offers for flexible combination of differing conceptual models.



Spatial use
-----------

During a simulation, roughly the following steps are taken:

1. The input are divided into chunks, based on a management area identifier.
2. Dask creates a graph detailing how to process the chunks.
3. Numba generates compiled code for the consolidation and oxidation algorithms.

Then, the graph is executed:

4. A chunk is passed on to the numba accelerated functions. The functions loop
   over the soil columns in a chunk, calculating consolidation and oxidation
   for one column at a time.
5. The results of the columns are written back to a spatial grid.
6. The spatial grids are collected in an xarray.DataSet and written to a netCDF
  file.

This is repeated untill all chunks have been processed. Step 2 and 3 result in
roughly 30 seconds overhead.

For very small datasets, the JIT overhead might be larger than straight up
computation time. In that case, JIT-compilation can be temporarily disabled by
setting an environmental variable, by running:

    $ SET NUMBA_DISABLE_JIT=1

(Closing the shell/prompt in question will delete the environmental variable,
and enable JIT-compilation again.)

A few notes on numba
--------------------

For speedy calculation, numpy is generally sufficient, but comes with some
limitations:

* All operations must be vectorized (looping over the elements of arrays is
  slow). While automatic broadcasting generally greatly simplifies code, some
  algorithms are very difficult to write in vectorized form. Additionally, the
  resulting code can be very convoluted, compared to a straightforward procedural
  implementation.
* By default, numpy creates copies rather than views of arrays for computation.
  This does not work well with large arrays, which we might want to mutate in
  place instead.

Fortunately, numba allows us to write fast loops in Python, with the aid of
JIT-compilation. Numba is heavily focused on numerical computing and very well
integrated with numpy arrays, but it still lacks many features. To name a few:

* No string processing
* No dictionaries

The consequence is that work-arounds have to be found to fill for missing
features. In particular:

* No classes are used to hold data and methods.
  Instead, within `atlans.datatypes` numpy.dtype is been used to define
  structured arrays to hold state and input data within the numba functions. The
  functions take and return these arrays rather than mutating their object's
  state. There are at least two benefits:
    - a more functional approach is mandatory.
    - the code looks more familiar to those without OO-programming experience.
* Lookup tables are used to translate numerical geology and lithological codes
  to parameter values. As no dictionaries are supported, the lookup tables are
  implemented using arrays: the array indices function as keys. The resulting
  "lookup arrays" are sparsely populated, but still relatively small in size
  compared to the data chunks of lithology and thickness.

Miscellaneous
-------------

* In numba, the numpy.dtype objects can be mutated using indexing: `[:]`
* In contrast to regular Python, numba will not treat a shape (1,) numpy array
  as a scalar, and it will raise a TypingError when you attempt to use it as a
  scalar.
* It's not possible to initialize a scalar numpy.dtype. Instead, initalize it
  with shape (1,) and then take slice `[0]`. The result is a structured "array"
  with scalar values.
* Keep row-major or column-major ordering in mind, numpy is by default C-ordered,
  which is row-major.
* Both dask and numba have options to parallelize code. In case of dask, the
  chunks will be processed in parallel. In case of numba, loops can be
  parallelized using `numba.prange()`.